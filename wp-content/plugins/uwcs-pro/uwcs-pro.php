<?php
/**
 * Plugin Name: User Wallet Credit System Pro
 * Version: 1.0.2
 * Description: User Wallet Credit System Pro Add-on
 * Author: Dash10 Digital
 * Author URI: https://dash10.digital
 */

define( 'UWCS_PRO', true );
define( 'UWCS_PRO_VERSION', '1.0.2' );
define( 'UWCS_PRO_FILE', __FILE__ );

define( 'AUTHOR_STORE_URL', 'https://dash10.digital' );
define( 'UWCS_PRO_ITEM_ID', 80 );

include_once( dirname( __FILE__ ) . '/functions.php' );
include_once( dirname( __FILE__ ) . '/includes/filters.php' );
include_once( dirname( __FILE__ ) . '/includes/shortcodes.php' );
include_once( dirname( __FILE__ ) . '/updater.php' );

/**
 * Class WC_Settings_Tab_Demo
 */
class UWCS_Pro_Settings {
	/**
	 * Bootstraps the class and hooks required actions & filters.
	 *
	 */
	public static function init() {
		add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
		add_action( 'woocommerce_settings_tabs_uwcs_settings', __CLASS__ . '::settings_tab' );
		add_action( 'woocommerce_update_options_uwcs_settings', __CLASS__ . '::update_settings' );
	}


	/**
	 * Add a new settings tab to the WooCommerce settings tabs array.
	 *
	 * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
	 *
	 * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
	 */
	public static function add_settings_tab( $settings_tabs ) {
		$settings_tabs['uwcs_settings'] = __( 'User Wallet', 'uwcs-pro' );

		return $settings_tabs;
	}

	/**
	 * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
	 *
	 * @uses woocommerce_admin_fields()
	 * @uses self::get_settings()
	 */
	public static function settings_tab() {
		woocommerce_admin_fields( self::get_settings() );
	}

	/**
	 * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
	 *
	 * @uses woocommerce_update_options()
	 * @uses self::get_settings()
	 */
	public static function update_settings() {

		woocommerce_update_options( self::get_settings() );

		// retrieve the license from the database
		$license = trim( get_option( 'uwcs_license_key_pro' ) );

		$api_params = array(
			'edd_action' => 'activate_license',
			'license'    => $license,
			'item_id'    => '80',
			'url'        => home_url()
		);
		// Call the custom API.
		$response = wp_remote_post( 'https://dash10.digital', array(
			'timeout'   => 15,
			'sslverify' => false,
			'body'      => $api_params
		) );
		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
			$message = ( is_wp_error( $response ) && ! empty( $response->get_error_message() ) ) ? $response->get_error_message() : __( 'An error occurred, please try again.' );
		} else {
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			if ( false === $license_data->success ) {
				switch ( $license_data->error ) {
					case 'expired' :
						$message = sprintf(
							__( 'Your license key expired on %s.' ),
							date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
						);
						break;
					case 'revoked' :
						$message = __( 'Your license key has been disabled.' );
						break;
					case 'missing' :
						$message = __( 'Invalid license.' );
						break;
					case 'invalid' :
					case 'site_inactive' :
						$message = __( 'Your license is not active for this URL.' );
						break;
					case 'item_name_mismatch' :
						$message = sprintf( __( 'This appears to be an invalid license key for %s.' ), 'UWCS Pro' );
						break;
					case 'no_activations_left':
						$message = __( 'Your license key has reached its activation limit.' );
						break;
					default :
						$message = __( 'An error occurred, please try again.' );
						break;
				}
			}
		}

		update_option( 'uwcs_license_key_pro_status', $license_data->license );
	}

	/**
	 * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
	 *
	 * @return array Array of settings for @see woocommerce_admin_fields() function.
	 */
	public static function get_settings() {
		$settings = array(
			'section_title'                                    => array(
				'name' => __( 'General', 'uwcs-pro' ),
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_uwcs_title'
			),
			'wc_settings_uwcs_show_credit_table_to_all' => array(
				'name'  => __( 'Allow products to be displayed to users that are not logged in', 'uwcs-pro' ),
				'type'  => 'checkbox',
				'value' => 'show',
				'desc'  => __( 'The user must be logged in before the purchase is made. Guest checkout will not work.', 'uwcs-pro' ),
				'id'    => 'wc_settings_uwcs_show_credit_table_to_all'
			),
			'button_text'                                      => array(
				'name' => __( 'Button Text', 'uwcs-pro' ),
				'type' => 'text',
				'desc' => __( 'Custom button text will override default button text', 'uwcs-pro' ),
				'id'   => 'wc_settings_uwcs_button_text'
			),
			'wc_settings_uwcs_checkout_order_status'           => array(
				'name'    => __( 'Order status for purchases made using funds', 'uwcs-pro' ),
				'type'    => 'select',
				'options' => apply_filters( 'uwcs_checkout_order_statuses', array(
					'completed' => 'completed',
					'on-hold'   => 'on-hold'
				) ),
				'desc'    => __( 'Order status of purchase using virtual funds.', 'uwcs-pro' ),
				'id'      => 'wc_settings_uwcs_checkout_order_status'
			),
			'checkout_error_currency_purchase'                 => array(
				'name' => __( 'Wallet Checkout Error Message', 'uwcs-pro' ),
				'type' => 'textarea',
				'desc' => __( 'This error message will display if a user tries to purchase funds using funds.', 'woocommerce-settings-tab-demo' ),
				'id'   => 'wc_settings_checkout_error_currency_purchase'
			),
			'insufficient_funds'                               => array(
				'name' => __( 'Insufficient Funds Error', 'uwcs-pro' ),
				'type' => 'textarea',
				'desc' => __( 'This error message will display in the event of insufficient funds during checkout.', 'woocommerce-settings-tab-demo' ),
				'id'   => 'wc_settings_insufficient_funds'
			),
			'uwcs_license_key_pro'                             => array(
				'name' => __( 'Plugin License', 'uwcs-pro' ),
				'type' => 'text',
				'desc' => strtoupper( get_option( 'uwcs_license_key_pro_status' ) ),
				'id'   => 'uwcs_license_key_pro'
			),
			'section_end'                                      => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_uwcs_section_end'
			)
		);

		return apply_filters( 'wc_settings_uwcs_settings', $settings );
	}
}

if ( defined( 'WPUW_FILE' ) ) {
	UWCS_Pro_Settings::init();
}

if ( ! class_exists( 'UWCS_Pro_Updater' ) ) {
	include_once( dirname( __FILE__ ) . '/updater.php' );
}

$license_key = get_option( 'uwcs_license_key_pro' );
$edd_updater = new UWCS_Pro_Updater( AUTHOR_STORE_URL, __FILE__, array(
	'version' => UWCS_PRO_VERSION,
	'license' => trim( $license_key ),
	'item_id' => UWCS_PRO_ITEM_ID,
	'author'  => 'Dash10 Digital',
	'url'     => home_url(),
	'beta'    => false
) );