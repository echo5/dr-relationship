<?php
/**
 * UWCS Pro Shortcodes
 *
 * @author Justin Greer <justin@dash10.digital>
 * @package UWCS Pro
 */

// [bartag foo="foo-value"]
function uwcs_dynamic_deposit_template_callback( $atts ) {
	extract( shortcode_atts( array(
		'button_text'    => 'Confirm Amount',
		'default_amount' => '25.00',
		'description'    => 'There is now description'
	), $atts ));

	if ( ! is_user_logged_in() ) {
		return 'Please log in before making a deposit';
	}

	$return = '
	<form action="" method="post">
		' . wp_nonce_field( 'uwcs_custom_deposit' ) . '
		<input type="hidden" name="action" value="uwcs_custom_deposit"/>
		
		<table id="uwcs-dynamic-load-table">
			<tr>
				<td>
				Deposit Amount<br/>
				<small class="description">' . $description . '</small>
				</td>
				<td><input type="number" name="deposit_amount" placeholder="Deposit Amount" value="' . $default_amount. '" required/></td>
			</tr>
		</table>
	    
	    <input type="submit" value="' . $button_text . '"/>
	</form>';

	$template = apply_filters( 'uwcs_dynamic_deposit_template', $return );

	return $template;
}

add_shortcode( 'uwcs_dynamic_deposit_template', 'uwcs_dynamic_deposit_template_callback' );

add_action( 'wp_loaded', 'uwcs_custom_deposit_callback' );
function uwcs_custom_deposit_callback() {
	if ( isset( $_POST['action'] ) && $_POST['action'] == 'uwcs_custom_deposit' ) {
		if ( ! wp_verify_nonce( $_POST['_wpnonce'], 'uwcs_custom_deposit' ) || ! is_user_logged_in() ) {
			return;
		}

		uwcs_create_dynamic_wallet_product( array(
			'name'     => get_current_user_id(),
			'tc_price' => sanitize_text_field( $_POST['deposit_amount'] )
		) );
	}
}