<?php
/**
 * User Wallet Credit System Pro Add-On
 *
 * @author Justin Greer <justin@dash10.digital>
 * @package UWCS Pro
 */

/**
 * Transfer funds to another user
 *
 * @param $from
 * @param $to
 * @param $amount
 *
 * @return boolean True of the process was successfull
 */
function uwcs_tranfer_funds( $from, $to, $amount ) {

	if ( ! defined( 'UWCS_PRO' ) ) {
		return;
	}

	// Check to ensure that both users exists
	$from = get_userdata( $from );
	$to   = get_userdata( $to );

	// The user MUST exists to make the transfer
	if ( ! $from || ! $to ) {
		return false;
	}

	// Get user funds
	$from_fund = floatval( get_user_meta( $from->ID, "_uw_balance", true ) );
	$to_fund   = floatval( get_user_meta( $to->ID, "_uw_balance", true ) );

	// Check if the funds exists
	if ( floatval( $amount ) > $from_fund ) {
		return false;
	}

	// Make the transfer
	$new_fund_amount_to   = ( $to_fund + $amount );
	$new_fund_amount_from = ( $from_fund - $amount );

	// Update the users funds
	update_user_meta( $from->ID, "_uw_balance", $new_fund_amount_from );
	update_user_meta( $to_fund, '_uw_balance', $new_fund_amount_to );

	// Allow a hook here for logging
	do_action( 'uwcs_tranfer_funds_complete', $from, $to, $amount );

	return true;
}

/**
 * Checkout Status Override
 *
 */
add_filter( 'wpuw_update_status', 'uwcs_checkout_order_status_fitler', 1 );
function uwcs_checkout_order_status_fitler( $default ) {
	if ( ! defined( 'UWCS_PRO' ) ) {
		return $default;
	}

	$status = strtolower( get_option( 'wc_settings_uwcs_checkout_order_status' ) );
	if ( $status ) {
		if ( $status == 'default' ) {
			return $default;
		}

		return $status;
	}

	return $default;
}

/**
 * Check if the product is dynamic
 *
 * @param $product_id
 *
 * @return bool
 */
function uwcs_is_dynamic_product( $product_id ) {
	$dynamic = get_post_meta( $product_id, '_uwcs_dynamic_product', true );

	if ( $dynamic == 'yes' ) {
		return true;
	}

	return false;
}

/**
 * Create custom deposit product
 *
 * @param $params
 */
function uwcs_create_dynamic_wallet_product( $params ) {

	//grab the values
	$pName        = $params["name"];
	$pDescription = 'Wallet Deposit';
	$pPrice       = $params["tc_price"];
	$post_title   = 'Fund Deposit ' . rand( 1000, 9999 ) . ' - ' . floatval( $params["tc_price"] ) . ' Credits';

	$post = array(
		'post_author'  => $pName,
		'post_content' => $pDescription,
		'post_status'  => "publish",
		'post_title'   => $post_title,
		'post_type'    => "product",
	);

	// create product
	$product_id = wp_insert_post( $post, __( 'Cannot create product', 'bones' ) );

	// type of product
	wp_set_object_terms( $product_id, 'simple', 'product_type' );

	// Required for the credit to process correctly
	wp_set_object_terms( $product_id, array( 'credit', 'dynamic_uwcs' ), 'product_cat' );

	//add price to the product, this is where you can add some descriptions such as sku's and measurements
	update_post_meta( $product_id, '_regular_price', $pPrice );
	update_post_meta( $product_id, '_sale_price', $pPrice );
	update_post_meta( $product_id, '_price', $pPrice );

	// Specific to the product. $1 is equal to 1 credit
	update_post_meta( $product_id, '_credits_amount', floatval( $params["tc_price"] ) );
	update_post_meta( $product_id, '_uwcs_dynamic_product', 'yes' );

	// Add the product to the cart
	global $woocommerce;

	$woocommerce->cart->add_to_cart( $product_id, 1 );

	// Redirect to the checkout page.
	$checkout_url = wc_get_checkout_url();
	wp_safe_redirect( $checkout_url );
	exit;
}

// Remove Dynamic Products from shop when the item is removed from the shopping cart
function uwcs_cart_removal( $cart_item_key, $cart ) {
	$product_id = $cart->cart_contents[ $cart_item_key ]['product_id'];
	if ( uwcs_is_dynamic_product( $product_id ) ) {
		wp_delete_post( $product_id, true );
	}
}

add_action( 'woocommerce_remove_cart_item', 'uwcs_cart_removal', 10, 2 );