<?php
/**
 * WooCommerce Memberships
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Memberships to newer
 * versions in the future. If you wish to customize WooCommerce Memberships for your
 * needs please refer to https://docs.woocommerce.com/document/woocommerce-memberships/ for more information.
 *
 * @package   WC-Memberships/Classes
 * @author    SkyVerge
 * @copyright Copyright (c) 2014-2017, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

 // Exit if accessed directly.
 defined( 'ABSPATH' ) || exit;

/**
 * Integration class for Appointments plugin
 *
 * @since 3.2.0
 */
class WC_Appointments_Integration_Memberships {


	/**
	 * Constructor
	 *
	 * @since 3.2.0
	 */
	public function __construct() {
		// Adjusts discounts.
		add_filter( 'appointment_form_calculated_appointment_cost', array( $this, 'adjust_appointment_cost' ), 10, 3 );
	}


	/**
	 * Adjust appointment cost
	 *
	 * @since 3.2.0
	 * @param float $cost
	 * @param \WC_Appointment_Form $form
	 * @param array $posted
	 * @return float
	 */
	public function adjust_appointment_cost( $appointment_cost, $appointment_form, $posted ) {

        // Need to separate extra costs (staff, addons) from product price.
        // Product price is already discounted_cost
        $extra_cost      = $appointment_cost - $appointment_form->product->get_price();
        $extra_cost      = wc_memberships()->get_member_discounts_instance()->get_discounted_price( $extra_cost, $appointment_form->product );
        $discounted_cost = $appointment_form->product->get_price() + $extra_cost;

        $member_discounts = wc_memberships()->get_member_discounts_instance();

        // Don't discount the price when adding an appointment to the cart.
		if ( doing_action( 'woocommerce_add_cart_item_data' ) ) {
            // Exists from WC Memberships 1.8.8 onwards.
            if ( method_exists( $member_discounts, 'get_original_price' ) ) {
                $discounted_cost = $member_discounts->get_original_price( $discounted_cost, $appointment_form->product );
            } else {
                $discounted_cost = $appointment_form->product->get_regular_price();
            }
        } elseif ( is_admin() && isset( $posted['add_appointment_2'] ) ) {
			$discounted_cost = $member_discounts->get_discounted_price( $appointment_cost, $appointment_form->product );
		}

		/*
		if ( doing_action( 'woocommerce_add_cart_item_data' ) ) {
			$discounted_cost = $cost;
		} else {
			$discounted_cost = wc_memberships()->get_member_discounts_instance()->get_discounted_price( $appointment_cost, $form->product );
		}
		*/

		return is_numeric( $discounted_cost ) ? (float) $discounted_cost : (float) $appointment_cost;
	}

}

$GLOBALS['wc_appointments_integration_memberships'] = new WC_Appointments_Integration_Memberships();
