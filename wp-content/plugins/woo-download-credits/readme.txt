=== Woo Credits ===
Contributors: brandonmuth,pankajagrawal
Donate link: http://www.woocredits.com/
Tags: wooCommerce, woocommerce extension, woocommerce payment gateway, woocommerce credit system, credits, download credits, woocommerce credits, digital downloads, download, downloads, e-commerce
Requires at least: 4.5
Tested up to: 4.8.1
Stable tag: 1.2.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A WordPress plugin that lets you create credit packages, which your customers purchase, then redeem towards products in your Woo Commerce store.

== Description ==
= A must have for Woo Commerce store owners! =
Woo Credits is a versatile WordPress plugin that lets you easily sell and accept credits in your WooCommerce store. 

= Capture More Revenue, Earn More Repeat Visitors =
Woo Credits makes your site work like iStockPhoto or other credit based sites. When customers purchase credits, they have increased incentive to return to your site to spend those credits and make sure the unused credits don't go to waste.

Visit WooCredits.com to upgrade to Platinum or investigate the screenshots below to see how it works. Platinum version works with Simple, Variable, Downloadable and Virtual products and also integrates with other popular WordPress plugins such as Woo Bookings, Woo Subscriptions, Woo Lottery, and WP Events Tickets Plus.

== Installation ==

You may directly upload the zip folder from admin or place the extracted files in wp-content/plugins directory. Once installed, navigate to Woo Commerce > Download Credits to set your pricing / credits structure. Then, on each individual Woo product, look for the Credits Required for Download field and enter the value of your choosing.

== Frequently Asked Questions ==



== Screenshots ==

1. screenshot-1
2. screenshot-2
3. screenshot-3
4. screenshot-4
5. screenshot-5
6. screenshot-6
7. screenshot-7
8. screenshot-8
9. screenshot-9
10. screenshot-10


== Changelog ==

= 1.2.1 =
* "Some bug fixes"

= 1.2.0 =
* "Product edit screen issues and Some minor bug fixes"

= 1.1.1 =
* "Some minor fixes"

= 1.1.0 =
* "fixed issues of woocommerce 3.x updates"

= 1.0.6 =
* "fix to have role based pricing appear for plugin WooCommerce Role Based Price"

= 1.0.5 =
* "some fixes to variable products pricing display"

= 1.0.3 =
* "Fix for duplicate credit wording that appeared in Woo Commerce 2.6+"

= 1.0.2 =
* "fixing bug that was subtracting credits incorrectly"
