<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fusion
 */

?>

			</div><!-- #content -->

			<?php
			if (fusion_is_elementor_active() && $page_id = get_the_ID() && !get_theme_mod( 'particle_background', false )) {
				$elementor_page = \Elementor\PageSettings\Manager::get_page( get_the_ID() ); 
				$particle_background = $elementor_page->get_settings( 'particle_background' );
				if ($particle_background == 'yes') { ?>
					<div id="particles" data-colors="<?php echo $elementor_page->get_settings( 'particle_colors' ); ?>" data-count="<?php echo $elementor_page->get_settings( 'particle_count' ); ?>"></div>
				<?php } ?>
			<?php } ?>
			
		</div><!-- .barba-container -->
	</div><!-- #barba-wrapper -->

	<?php if (get_theme_mod( 'particle_background', false )) { ?>
		<div id="particles" data-colors="<?php echo get_theme_mod( 'particle_colors', 'red,blue' ); ?>" data-count="<?php echo get_theme_mod( 'particle_count', '20' ); ?>"></div>
	<?php } ?>


	<?php if ( is_active_sidebar( 'fixed-corner-left' ) ) { ?>
		<div class="fixed-corner-left">
			<?php dynamic_sidebar( 'fixed-corner-left' ); ?>
		</div>
	<?php } ?>

	<?php if ( is_active_sidebar( 'fixed-corner-right' ) ) { ?>
		<div class="fixed-corner-right">
			<?php dynamic_sidebar( 'fixed-corner-right' ); ?>
		</div>
	<?php } ?>

	<?php if ( is_active_sidebar( 'footer-1' ) && fusion_is_elementor_active() ) { ?>
		<footer id="footer" class="site-footer" role="contentinfo">
		  <?php dynamic_sidebar( 'footer-1' ); ?>
		</footer><!-- #footer -->
	<?php } ?>


<?php wp_footer(); ?>

</body>
</html>
