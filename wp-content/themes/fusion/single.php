<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package fusion
 */

get_header();

	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/post/single', fusion_get_post_single_style() );

	endwhile; // End of the loop.

get_footer();
