<?php

LuxeOption::add_section( 'advanced', array(
    'title'          => esc_attr__( 'Advanced', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

/**
 * Advanced
 */
 LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'minified_css',
    'label'       => esc_attr__( 'Enable minified CSS', 'fusion' ),
    'description' => esc_attr__( 'Use minified CSS instead of full size for faster loading.', 'fusion' ),
    'section'     => 'advanced',
    'default'     => false,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'minified_js',
    'label'       => esc_attr__( 'Enable minified Javascript', 'fusion' ),
    'description' => esc_attr__( 'Use minified Javascript instead of full size for faster loading.', 'fusion' ),
    'section'     => 'advanced',
    'default'     => true,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'code',
    'settings'    => 'custom_js',
    'label'       => esc_attr__( 'Custom Javascript', 'fusion' ),
    'description' => esc_attr__( 'Add any custom javascript here.  Do not include the <\script\> tags.', 'fusion' ),
    'section'     => 'advanced',
    'default'     => '',
    'priority'    => 10,
    'choices'     => array(
        'language' => 'javascript',
        'theme'    => 'monokai',
        'height'   => 250,
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'code',
    'settings'    => 'custom_head_scripts',
    'label'       => esc_attr__( 'Custom Head Scripts', 'fusion' ),
    'description'     => esc_attr__( 'Add custom scripts or styles in your head.  You can include the <\script\> tags.  This is a good place for TypeKit scripts, Google Analytics, or other third party scripts/styles.', 'fusion' ),
    'section'     => 'advanced',
    'default'     => '',
    'priority'    => 10,
    'choices'     => array(
        'language' => 'html',
        'theme'    => 'monokai',
        'height'   => 250,
    ),
) );
