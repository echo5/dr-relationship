<?php

LuxeOption::add_panel( 'nav', array(
    'title'          => esc_attr__( 'Navigation', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );
LuxeOption::add_section( 'nav_base', array(
    'title'          => esc_attr__( 'Base Navigation', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'nav'
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'navigation_bg',
    'label'       => esc_attr__( 'Mobile Navigation, Slide Out, &amp; Overlay Background Color', 'fusion' ),
    'description' => esc_attr__( 'Set the background color of your navigation overlay and mobile navigation.', 'fusion' ),
    'section'     => 'nav_base',
    'default'     => 'transparent',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'body[class*="header-style-side-"] .navbar-nav, .nav-overlay',
            'property' => 'background-color',
        ),
        array(
            'element'  => '#navbar',
            'property' => 'background-color',
            'media_query' => '@media screen and (max-width: 991px)',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => 'body[class*="header-style-side-"] #navbar, .nav-overlay',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'navigation_typography',
    'label'       => esc_attr__( 'Navigation Typography', 'fusion' ),
    'description' => esc_attr__( 'Typography for the header navigation.', 'fusion' ),
    'section'     => 'nav_base',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '14',
        'font-weight'    => '400',
        'line-height'    => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => false,
        'letter-spacing' => true,
        'color'          => false,
        'units'          => array( 'px', 'rem' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#navbar, .navbar-nav .nav-link',
        ),
    ),
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'navigation_submenu_bg',
    'label'       => esc_attr__( 'Submenu Background Color', 'fusion' ),
    'description' => esc_attr__( 'Set the background color of your dropdown and submenu colors.', 'fusion' ),
    'section'     => 'nav_base',
    'default'     => 'transparent',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '#nav-primary.navbar-nav .dropdown-menu',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '#nav-primary.navbar-nav .dropdown-menu',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'navigation_submenu_typography',
    'label'       => esc_attr__( 'Submenu Typography', 'fusion' ),
    'description' => esc_attr__( 'Typography for the submenu navigation.', 'fusion' ),
    'section'     => 'nav_base',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '14',
        'font-weight'    => '400',
        'line-height'    => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color' => '#191919',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => false,
        'letter-spacing' => true,
        'color'          => true,
        'units'          => array( 'px', 'rem' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#nav-primary.navbar-nav .dropdown-menu .nav-link',
        ),
    ),
) );
