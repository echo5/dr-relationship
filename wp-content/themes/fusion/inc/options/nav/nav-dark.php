<?php

LuxeOption::add_section( 'nav_dark', array(
    'title'          => esc_attr__( 'Dark Background Navigation', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'nav'
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'nav_typography_dark_color',
    'label'       => esc_attr__( 'Dark Background Navigation Font Color', 'fusion' ),
    'section'     => 'nav_dark',
    'default'     => '#f1f1f1',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-dark-active .navbar-nav .nav-link',
            'property' => 'color',
        ),
    ),
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'nav_typography_dark_color_hover',
    'label'       => esc_attr__( 'Dark Navigation Font Hover Color', 'fusion' ),
    'section'     => 'nav_dark',
    'default'     => '#ffffff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-dark-active .navbar-nav .nav-link:hover, .header-dark-active .navbar-nav .current-menu-item .nav-link',
            'property' => 'color',
        ),
    ),
) );