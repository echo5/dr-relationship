<?php

LuxeOption::add_section( 'forms', array(
    'title'          => esc_attr__( 'Forms', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );

$elements = array();
$elements[] = '.form-group input';
$elements[] = 'input';
$elements[] = '.form-group textarea';
$elements[] = 'textarea';
$elements[] = '.form-group select';
$elements[] = 'select';

/**
 * Form inputs
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'input_bg_color',
    'label'       => esc_attr__( 'Input Background Color', 'fusion' ),
    'section'     => 'forms',
    'default'     => 'rgba(180,180,180,0)',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'input_bg_color_focus',
    'label'       => esc_attr__( 'Input Focus Background Color', 'fusion' ),
    'section'     => 'forms',
    'default'     => 'rgba(180,180,180,0)',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':focus, ', $elements),
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'input_border_width',
    'label'       => esc_attr__( 'Input Border Width', 'fusion' ),
    'description' => esc_attr__( 'Controls how wide your content is on larger screens.', 'fusion' ),
    'help'        => esc_attr__( 'This does not apply to full browser width sections.', 'fusion' ),
    'section'     => 'forms',
    'default'     => '1px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'border-width',
            // 'suffix' => '; border-style: solid'
        ),
    ),
    'choices' => array(
        'units' => array( 'px' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'input_border_color',
    'label'       => esc_attr__( 'Input Border Color', 'fusion' ),
    'section'     => 'forms',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'border-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'input_color_focus',
    'label'       => esc_attr__( 'Input Focus Color', 'fusion' ),
    'section'     => 'forms',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':focus, ', $elements),
            'property' => 'border-color',
        ),
        array(
            'element'  => '.form-group input:focus~label, .form-group textarea:focus~label',
            'property' => 'color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'input_typography',
    'label'       => esc_attr__( 'Input Typography', 'fusion' ),
    'section'     => 'forms',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '16px',
        'font-weight'    => '400',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#333333',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => false,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => false,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    // 'choices'     => Kirki_Fonts::get_font_choices(),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => implode(', ', $elements),
        ),
    ),
) );
