<?php

/**
 * Update global options to avoid theme conflict
 */
function fusion_update_elementor_global_option () {
  update_option('elementor_disable_color_schemes', 'yes');
  update_option('elementor_disable_typography_schemes', 'yes');
}
add_action('after_switch_theme', 'fusion_update_elementor_global_option');

/**
 * Add page settings
 */
if ( ! function_exists( 'fusion_add_elementor_page_settings' ) ) {
    function fusion_add_elementor_page_settings($element, $section_id, $args)
    {

        if ($section_id !== 'document_settings')
            return;

        $element->add_control(
            'header_style',
            [
                'label' => esc_html__( 'Header Style (from theme options)', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => '',
                'options' => [
                    '' => esc_html__( 'Default', 'fusion' ),
                    'dark' => esc_html__( 'Dark Background', 'fusion' ),
                    'light' => esc_html__( 'Light Background', 'fusion' ),
                ],
            ]
        );

        $element->add_control(
            'menu_item_color',
            [
                'label' => esc_html__( 'Menu Item Color', 'fusion' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .navbar a' => 'color: {{VALUE}}',
                ],
            ]
        );

        $element->add_control(
            'header_bg_color',
            [
                'label' => esc_html__( 'Header Background', 'fusion' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .navbar' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $element->add_control(
            'page_title_color',
            [
                'label' => esc_html__( 'Page Title Color', 'fusion' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} h1' => 'color: {{VALUE}}',
                ],
            ]
        );

        $element->add_control(
            'body_color',
            [
                'label' => esc_html__( 'Body Font Color', 'fusion' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}' => 'color: {{VALUE}}',
                ],
            ]
        );

        $element->add_control(
            'particle_background',
            [
                'label' => esc_html__( 'Particle Background', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'default' => '',
                'label_on' => esc_html__( 'On', 'fusion' ),
                'label_off' => esc_html__( 'Off', 'fusion' ),
                'return_value' => 'yes',
            ]
        );

        $element->add_control(
            'particle_count',
            [
                'label'   => esc_html__( 'Particle Count', 'fusion' ),
                'type'    => \Elementor\Controls_Manager::NUMBER,
                'default' => 20,
                'min'     => 1,
                'max'     => 300,
                'step'    => 5,
                'condition' => [
                    'particle_background' => 'yes',
                ],
            ]
        );

        $element->add_control(
            'particle_colors',
            [
                'label'       => esc_html__( 'Particle Colors', 'fusion' ),
                'type'        => \Elementor\Controls_Manager::TEXT,
                'default'     => '',
                'description'     => esc_html__( 'Colors separated by commas without spaces.  You can use color names or hex values here.', 'fusion' ),
                'placeholder' => esc_html__( 'red,#0080ff', 'fusion' ),
                'condition' => [
                    'particle_background' => 'yes',
                ],
            ]
        );

    }
}
add_action( 'elementor/element/before_section_end', 'fusion_add_elementor_page_settings',10, 3);


/**
 * Load Anywhere Elementor in head to prevent FOUC
 */
function fusion_enqueue_anywhere_elementor_scripts() {
  if (fusion_is_elementor_active()) {
      $args = array( 'post_type' => 'ae_global_templates', 'post_status' => array('publish', 'draft'));
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post();
          $post_id = get_the_ID();
          $css_file = new \Elementor\Post_CSS_File( $post_id );
          $css_file->enqueue();
      endwhile;
      wp_reset_postdata();
  }
}
add_action( 'wp_enqueue_scripts', 'fusion_enqueue_anywhere_elementor_scripts', 5 );

/**
 * Add Elementor admin scripts and styles
 */
function fusion_enqueue_elementor_admin_scripts() {
    wp_enqueue_style( 'fusion-elementor-admin', get_template_directory_uri() . '/elementor-admin.css' );
    if (get_theme_mod( 'elementor_detached', true )) {
        wp_enqueue_script( 'fusion-elementor-admin', get_template_directory_uri() . '/assets/js/elementor-admin.js' );
    }
}
add_action( 'elementor/editor/after_enqueue_scripts', 'fusion_enqueue_elementor_admin_scripts', 5 );

/**
 * Add thumbnail to templates library
 */
function fusion_change_template_library_template() {
    ?>
    <script type="text/template" id="tmpl-elementor-template-library-template-local">
        <div class="elementor-template-library-template-icon">
            <i class="fa fa-{{ 'section' === type ? 'columns' : 'file-text-o' }}"></i>
        </div>
        <div class="elementor-template-library-template-thumbnail">
            {{{ thumbnail ? '<img src="' + thumbnail + '">' : '<i class="fa fa-file-image-o"></i>' }}}
            <button class="elementor-template-library-template-action elementor-template-library-template-insert elementor-button elementor-button-success">
                <i class="eicon-file-download"></i><span class="elementor-button-title"><?php _e( 'Insert', 'fusion' ); ?></span>
            </button>
        </div>
        <div class="elementor-template-library-template-meta">
            <div class="elementor-template-library-template-name">{{{ title }}}</div>
            <div class="elementor-template-library-template-type">{{{ elementor.translate( type ) }}}</div>
        </div>
        <div class="elementor-template-library-template-controls">
            <div class="elementor-template-library-template-export">
                <a href="{{ export_link }}">
                    <i class="fa fa-sign-out"></i><span class="elementor-template-library-template-control-title"><?php echo __( 'Export', 'fusion' ); ?></span>
                </a>
            </div>
            <div class="elementor-template-library-template-delete">
                <i class="fa fa-trash-o"></i><span class="elementor-template-library-template-control-title"><?php echo __( 'Delete', 'fusion' ); ?></span>
            </div>
            <div class="elementor-template-library-template-preview">
                <i class="eicon-zoom-in"></i><span class="elementor-template-library-template-control-title"><?php echo __( 'Preview', 'fusion' ); ?></span>
            </div>
        </div>
    </script>
    <?php
}
add_action( 'elementor/editor/after_enqueue_scripts', 'fusion_change_template_library_template', 5 );
