<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package fusion
 */

if ( ! function_exists( 'fusion_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function fusion_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'fusion' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'fusion' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'fusion_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function fusion_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'fusion' ),
			'after'  => '</div>',
		) );
		
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'fusion' ) );
		if ( $categories_list && fusion_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'fusion' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'fusion' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'fusion' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'fusion' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'fusion' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
if ( ! function_exists( 'fusion_categorized_blog' ) ) :
	function fusion_categorized_blog() {
		if ( false === ( $all_the_cool_cats = get_transient( 'fusion_categories' ) ) ) {
			// Create an array of all the categories that are attached to posts.
			$all_the_cool_cats = get_categories( array(
				'fields'     => 'ids',
				'hide_empty' => 1,
				// We only need to know if there is more than one category.
				'number'     => 2,
			) );

			// Count the number of categories that are attached to the posts.
			$all_the_cool_cats = count( $all_the_cool_cats );

			set_transient( 'fusion_categories', $all_the_cool_cats );
		}

		if ( $all_the_cool_cats > 1 ) {
			// This blog has more than 1 category so fusion_categorized_blog should return true.
			return true;
		} else {
			// This blog has only 1 category so fusion_categorized_blog should return false.
			return false;
		}
	}
endif;

/**
 * Flush out the transients used in fusion_categorized_blog.
 */
function fusion_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'fusion_categories' );
}
add_action( 'edit_category', 'fusion_category_transient_flusher' );
add_action( 'save_post',     'fusion_category_transient_flusher' );

/**
 * Navigation button
 */
if ( ! function_exists( 'fusion_nav_button' ) ) :
	function fusion_nav_button($sequence = null)
	{
			$nav_button = get_theme_mod('nav_button', 'icon_and_text');
			$text = get_theme_mod('nav_button_text', 'Menu');
			$icon = '<i class="icon-menu"></i><i class="icon-cross2 close-icon"></i>';
			$content = '';
			if ($nav_button == 'text' || $nav_button == 'icon_and_text') {
					$content .= '<span class="close-toggler-text">'. esc_attr__( 'Close', 'fusion' ) .'</span>';
					$content .= '<span class="toggler-text">' . $text . '</span>';
			}
			if ($nav_button == 'icon' || $nav_button == 'icon_and_text') {
					$content .= $icon;
			}
			?>
			<button class="navbar-toggler navbar-style-<?php echo $nav_button; ?>" type="button" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation" data-sequence="<?php echo $sequence; ?>">
				<?php echo $content; ?>
			</button>
			<?php
	}
endif;

/**
 * Comment fields for bootstrap
 */
if ( ! function_exists( 'fusion_comment_fields' ) ) :
	function fusion_comment_fields( $fields ) {
		if (is_singular('post') || is_page()) {
			$comment_field = $fields['comment'];

			$fields   =  array(
				'author' => '<div class="row"><div class="col-md-6"><p class="comment-form-author form-group">' .
					'<input id="author" name="author" type="text" value="" size="30" maxlength="245"  class="required" /><label for="author">' . esc_html__( 'Name', 'fusion' ) . '<span class="required">*</span>' . '</label></p></div>',
				'email'  => '<div class="col-md-6"><p class="comment-form-email form-group">' .
					'<input id="email" name="email" ' . 'type="email"' . ' value="" size="30" maxlength="100" aria-describedby="email-notes" class="required" /><label for="email">' . esc_html__( 'Email', 'fusion' ) . '<span class="required">*</span>' . '</label></p></div></div>',
				'url'    => '<p class="comment-form-url form-group">' .
					'<input id="url" name="url" type="url" value="" size="30" maxlength="200" /><label for="url">' . esc_html__( 'Website', 'fusion' ) . '</label></p>',
				'comment' => '<p class="comment-form-comment form-group"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" class="required"></textarea><label for="comment">' . _x( 'Comment', 'noun', 'fusion' ) . '</label></p>'

			);
		}
		return $fields;
	}
	add_filter( 'comment_form_fields', 'fusion_comment_fields' );
endif;

/**
 * Get social share links
 * @param  int $page_id
 * @param  str $title   
 * @return str
 */
if ( ! function_exists( 'fusion_social_share' ) ) :
	function fusion_social_share($page_id, $title, $hover = false) {
			if (!fusion_is_elementor_active())
				return; 

			$url = get_permalink($page_id);
			$classes = array();
			$classes[] = $hover ? 'social-share-hover' : '';
			$content = '<div class="social-icons social-share ' . implode(' ', $classes) . '">';
			$content .= '<span class="social-share-label h5">'. esc_attr__( 'Share on', 'fusion' ) .'</span>';
			$content .= '<ul>';
			if ($hover) {
					$content .= '<li><i class="icon ion-android-share-alt share-icon"></i><ul>';
			}
			$content .= '<li class=""><a target="_blank" href="http://www.facebook.com/sharer.php?u='.$url.'"><i class="icon fa fa-facebook"></i><span class="social-name">'. esc_attr__( 'Facebook', 'fusion' ) .'</span></a></li>';
			$content .= '<li class=""><a target="_blank" href="https://twitter.com/share?url='.$url.'&text='.$title.'"><i class="icon fa fa-twitter"></i><span class="social-name">'. esc_attr__( 'Twitter', 'fusion' ) .'</span></a></li>';
			$content .= '<li class=""><a target="_blank" href="https://plus.google.com/share?url='.$url.'"><i class="icon fa fa-google"></i><span class="social-name">'. esc_attr__( 'Google', 'fusion' ) .'</span></a></li>';
			$content .= '<li class=""><a target="_blank" href="http://pinterest.com/pin/create/button/?url='.$url.'&description='.$title.'"><i class="icon fa fa-pinterest-p"></i><span class="social-name">'. esc_attr__( 'Pinterest', 'fusion' ) .'</span></a></li>';
			if ($hover) {
				$content .= '</ul>';
			}
			$content .= '</li></ul>';
			$content .= '</div>';

			return $content;
	}
endif;

/**
 * Post navigation styling
 */
if ( ! function_exists( 'fusion_post_navigation' ) ) :
	function fusion_post_navigation() {
		echo get_the_post_navigation(array(
			'prev_text'          => '<small>'. esc_attr__( 'Previous Article', 'fusion' ) .'</small><span class="article-name">%title</span>',
			'next_text'          => '<small>'. esc_attr__( 'Next Article', 'fusion' ) .'</small><span class="article-name">%title</span>',
		));
	}
endif;
if ( ! function_exists( 'fusion_post_navigation_minimal' ) ) :
	function fusion_post_navigation_minimal() {
		echo get_the_post_navigation(array(
			'prev_text'          => '<span class="article-name slide-in">%title</span>',
			'next_text'          => '<span class="article-name slide-in">%title</span>',
		));
	}
endif;

/**
 * Search form
 */
function fusion_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
    <div class="form-group">
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <label for="s">' . esc_html__( 'Enter Keyword', 'fusion' ) . '</label>
    <input type="submit" id="searchsubmit" value="'. esc_attr__( 'Search', 'fusion' ) .'" />
    </div>
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'fusion_search_form', 100 );
