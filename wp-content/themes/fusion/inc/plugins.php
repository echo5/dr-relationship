<?php

/**
 * Recommended Plugins
 */
function fusion_register_required_plugins() {
	/*
	 * Array of plugin arrays
	 */
	$plugins = array(

		array(
			'name'               => esc_html__('ThemeLuxe Post Types', 'fusion'),
			'slug'               => 'luxe-post-types',
			'source'             => get_template_directory() . '/inc/plugins/luxe-post-types.zip',
			'required'           => true,
			'version'            => '',
			'force_activation'   => false,
			'force_deactivation' => false,
			'external_url'       => '',
			'is_callable'        => '',
		),

		array(
			'name'      => esc_html__('One Click Demo Import', 'fusion'),
			'slug'      => 'one-click-demo-import',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('Kirki Toolkit', 'fusion'),
			'slug'      => 'kirki',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('Elementor', 'fusion'),
			'slug'      => 'elementor',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('AnyWhere Elementor', 'fusion'),
			'slug'      => 'anywhere-elementor',
			'required'  => false,
		),
		array(
			'name'      => esc_html__('WooCommerce', 'fusion'),
			'slug'      => 'woocommerce',
			'required'  => false,
		),
		array(
			'name'      => esc_html__('Contact Form 7', 'fusion'),
			'slug'      => 'contact-form-7',
			'required'  => false,
		),
		
	);

	/*
	 * Configuration settings
	 */
	$config = array(
		'id'           => 'fusion-tgmpa',
		'default_path' => '',                      
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'themes.php',
		'capability'   => 'edit_theme_options',
		'has_notices'  => true,
		'dismissable'  => true,
		'dismiss_msg'  => '',
		'is_automatic' => true,
		'message'      => '',
		/*
		'strings'      => array(
			'page_title'                      => esc_html__( 'Install Required Plugins', 'theme-slug' ),
			'menu_title'                      => esc_html__( 'Install Plugins', 'theme-slug' ),
			// <snip>...</snip>
			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		)
		*/
	);

	tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'fusion_register_required_plugins' );