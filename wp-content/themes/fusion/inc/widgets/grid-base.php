<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

abstract class Widget_Grid_Base extends Widget_Base {

	public function get_categories() {
		return [ 'fusion-widgets' ];
	}

	public function get_icon() {
		return 'eicon-posts-grid';
	}

	public function is_reload_preview_required() {
		return true;
	}

	protected function grid_controls() {

		$default_grid_options = array(
			'' => esc_html__( 'Basic Grid', 'fusion' ),
			'masonry' => esc_html__( 'Masonry Columns', 'fusion' ),
		);
		$grid_options = apply_filters( 'luxe_grid_style_options', $default_grid_options );
		$this->add_control(
			'grid_style',
			[
				'label' => esc_html__( 'Grid Style', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'options' => $grid_options,
				'default' => '',
			]
		);

		$this->add_control(
			'columns',
			[
				'label' => esc_html__( 'Columns', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
				),
				'condition' => [
					'grid_style' => array('', 'masonry'),
				],
			]
		);

		$this->add_control(
			'gap',
			[
				'label' => esc_html__( 'Gap', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 100,
					],
				],
				'default' => [
					'size' => 30,
				],
				'show_label' => true,
				'selectors' => [
					'{{WRAPPER}} .luxe-grid' => 'margin-left: -{{SIZE}}{{UNIT}}; margin-right: -{{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .grid-item' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}}; padding-bottom: calc({{SIZE}}{{UNIT}}*2);',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'image',
				'condition' => [
					'grid_style' => array('', 'masonry'),
				],
			]
		);

		$this->add_control(
			'pagination',
			[
				'label' => esc_html__( 'Pagination', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => esc_html__( 'None', 'fusion' ),
					'links' => esc_html__( 'Links', 'fusion' ),
					// 'infinite' => esc_html__( 'Infinite Scroll', 'fusion' ),
				],
			]
		);

		$this->add_control(
		    'active_items',
		    [
		        'label' => esc_html__( 'Active Items', 'fusion' ),
		        'type' => \Elementor\Controls_Manager::TEXT,
		        'description' => 'A list of comma separated numbers of items to include. (e.g., To add an active class to the 1st and 3rd item you would write "1,3").'
		    ]
		);

		$this->add_control(
		    'animate_items',
		    [
		        'label' => esc_html__( 'Animate Items', 'fusion' ),
		        'type' => \Elementor\Controls_Manager::SWITCHER,
		        'default' => '',
		        'label_on' => esc_html__( 'On', 'fusion' ),
		        'label_off' => esc_html__( 'Off', 'fusion' ),
		        'return_value' => 'yes',
		    ]
		);

		$this->add_control(
		    'grid_item_animation_duration',
		    [
		        'label'   => esc_html__( 'Animation Duration', 'fusion' ),
		        'type'    => \Elementor\Controls_Manager::NUMBER,
		        'default' => 1200,
		        'min'     => 100,
		        'max'     => 3000,
		        'step'    => 100,
		        'condition' => [
		            'animate_items' => 'yes',
		        ],
		    ]
		);

		$default_animations = array(
		    '' => esc_html__( 'None', 'fusion' ),
		);
		$animation_options = apply_filters( 'luxe_animation_options', $default_animations );

		$this->add_control(
		    'grid_item_animation',
		    [
		        'type' => \Elementor\Controls_Manager::SELECT,
		        'label' => esc_html__( 'Animation', 'fusion' ),
		         'default' => 'transition.fadeIn',
		         'options' => $animation_options,
		         'condition' => [
		             'animate_items' => 'yes',
		         ],
		    ]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => esc_html__( 'Additional Items', 'fusion' ),
			]
		);

		$this->add_control(
			'insert_items',
			[
				'label' => esc_html__( 'Items', 'fusion' ),
				'type' => Controls_Manager::REPEATER,
				'prevent_empty' => false,
				'default' => array(),
				'fields' => [
					[
						'name' => 'position',
						'label' => esc_html__( 'Position', 'fusion' ),
						'type' => Controls_Manager::NUMBER,
						'default' => 0,
						'show_label' => true,
					],
					[
						'name' => 'content',
						'label' => esc_html__( 'Content', 'fusion' ),
						'type' => Controls_Manager::TEXTAREA,
						'default' => esc_html__( 'Grid Item Content', 'fusion' ),
						'show_label' => true,
					],
				],
				// 'title_field' => '{{{ tab_title }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_overlay',
			[
				'label' => esc_html__( 'Overlay', 'fusion' ),
			]
		);


		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background_overlay',
				'types' => [ 'none', 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} .grid-item .overlay',
			]
		);

		$this->end_controls_section();

	}

	protected function get_grid_style($name) {
		$grid_styles = apply_filters( 'luxe_grid_styles', array() );
		return $grid_styles[$name];
	}

	public function get_script_depends() {
		return [ 'fusion-main' ];
	}

	protected function render_grid($items, $settings, $post_type = '') {

		if ( $post_type) {
			$query = fusion_get_post_items_by_settings( $settings, $post_type );
			$items = $query->posts;
		} 

		if ( !empty($settings['insert_items']) ) {
			foreach ($settings['insert_items'] as $item) {
				if (count($items)) {
					array_splice( $items, absint($item['position']), 0, array($item['content']) );
				}
				else {
					$items[] = $item['content'];
				}
			}
		}

		if ( empty( $items ) ) {
			return;
		}

		$grid_options = [
		];

		$grid_classes = [ 'luxe-grid', 'row', 'gap-wide' ];

		if ( !empty($settings['post_type_style']) ) {
			$grid_classes[] = $settings['post_type_style'];
		}
		if ($settings['grid_style'] != '') {
			$grid_classes[] = 'luxe-grid-masonry';
			$grid_classes[] = 'grid-style-' . $settings['grid_style'];
		}


		$grid_style_count = 0;
		if (!empty($settings['grid_style']) && $settings['grid_style'] != '') {
			$grid_style = $this->get_grid_style($settings['grid_style']);
			$grid_style_count = count($grid_style);
		} 

		// Only applied to masonry and basic
		switch($settings['columns']) {
			case '1':
				$columns_class = 'col-md-12';
				break;
			case '2':
				$columns_class = 'col-md-6';
				break;
			case '3':
				$columns_class = 'col-md-4 col-sm-6';
				break;
			case '4':
				$columns_class = 'col-lg-3 col-md-4 col-sm-6';
				break;
		}


		?>
		<div class="elementor-grid-wrapper">
			<div class="<?php echo implode( ' ', $grid_classes ); ?>" data-grid_options='<?php echo esc_attr( wp_json_encode( $grid_options ) ); ?>'>
					<?php $n = 0; ?>
					<!-- <div class="grid-item col-md-4 height-1"></div> -->
					<?php if ($post_type) global $post; ?>
					<?php foreach ($items as $index => $post): ?>
						<?php
							$grid_item_classes = array();
							// Active class items
							$active_items = explode(',', $settings['active_items']);
							if (is_array($active_items)  && in_array($index + 1, $active_items)) {
								$grid_item_classes[] = 'active';
							}
							// Animation
							if ($settings['animate_items'] == 'yes') {
								$grid_item_classes[] = 'has-animation animate-child animation-stagger';
							}
							// Grid style
							if (isset($grid_style)) {
								$grid_item_classes[] = $grid_style[$n]['classes'];
							}
							if (!isset($grid_style) || $settings['grid_style'] == 'masonry') {
								$grid_item_classes[] = $columns_class;
							}
							// Post vs item
							if ( !$post_type || !is_a($post, 'WP_Post') ) {
								$grid_item_classes[] = 'grid-item-insert';
							}
						?>
						<div class="grid-item <?php echo implode(' ', $grid_item_classes); ?>" data-animation="<?php echo $settings['grid_item_animation']; ?>" data-animation-duration="<?php echo $settings['grid_item_animation_duration']; ?>" data-animation-stagger="500">
							<?php 
								$GLOBALS['image_size'] = isset($grid_style) ? $grid_style[$n]['image_size'] : $settings['image_size'];
								if ( $post_type && is_a($post, 'WP_Post') ): 
									setup_postdata($post);
									echo \fusion_load_template_part( $post->post_type . '/' . 'grid' );
								else:
									echo $post;
								endif;
								$n = ($grid_style_count == $n + 1) ? 0 : $n + 1;
							?>
						</div>
					<?php endforeach; ?>
			</div>
			<?php 
				if ($settings['pagination'] == 'links') {
					if (get_query_var('page')) {
						$paged = get_query_var('page');
					} elseif (get_query_var('paged')) {
						$paged = get_query_var('paged');
					} else {
						$paged = 1;
					}
			    $pagination = paginate_links( array(
			        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			        'total'        => $query->max_num_pages,
			        'current'      => max( 1, $paged ),
			        'format'       => '?paged=%#%',
			        'show_all'     => false,
			        'prev_next'    => true,
			        'prev_text'    => '<span class="icon icon-chevron-left"></span>',
			        'next_text'    => '<span class="icon icon-chevron-right"></span>',
			        'add_args'     => false,
			    ) );
		    ?>
		    <div class="pagination">
		    	<?php echo $pagination; ?>
		    </div>
		    <?php
			  }
			?>

		</div>
		<?php if ($post_type) wp_reset_postdata(); ?>
		<?php
	}


}
