<?php

/**
 * Image gallery addons
 */
function fusion_add_options_to_elementor_image_gallery($element, $args) {

	$element->add_control(
		'style' ,
		[
			'label'        => 'Style',
			'type'         => Elementor\Controls_Manager::SELECT,
			'default'      => 'default',
			'options'      => array( 'default' => 'Default', 'color-on-hover' => 'Color on Hover' ),
			'prefix_class' => 'image-gallery-style-',
			'label_block'  => true,
		]
	);

}
add_action( 'elementor/element/image-gallery/section_gallery/before_section_end', 'fusion_add_options_to_elementor_image_gallery', 10, 2);
