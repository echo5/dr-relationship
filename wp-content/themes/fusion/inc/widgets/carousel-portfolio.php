<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Portfolio_Carousel extends Widget_Carousel_Base {

	public function get_name() {
		return 'portfolio-carousel';
	}

	public function get_title() {
		return esc_html__( 'Portfolio Carousel', 'fusion' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_carousel',
			[
				'label' => esc_html__( 'Carousel', 'fusion' ),
			]
		);

		$portfolio_styles = apply_filters( 'luxe_portfolio_grid_styles', array('default' => 'Default'));
		$this->add_control(
			'portfolio_style',
			[
				'label' => esc_html__( 'Portfolio Style', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => $portfolio_styles,
			]
		);

		$this->carousel_controls();

	}

	protected function render() {
		$settings = $this->get_settings();
		$settings['post_type_style'] = 'portfolio-style-' . $settings['portfolio_style'];
		$GLOBALS['portfolio_grid_style'] = $settings['portfolio_style'];

		$this->render_carousel(null, $settings, 'portfolio');
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Portfolio_Carousel() );

