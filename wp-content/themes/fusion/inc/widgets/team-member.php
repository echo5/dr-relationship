<?php
namespace Elementor;

use Elementor\Widget_Base;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Team_Member extends Widget_Base {

	public function get_name() {
		return 'team_member';
	}

	public function get_title() {
		return esc_html__( 'Team Member', 'fusion' );
	}

	public function get_icon() {
		return 'eicon-person';
	}

	public function get_categories() {
		return [ 'fusion-widgets' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_custom_map',
			[
				'label' => esc_html__( 'Member', 'fusion' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label' => esc_html__( 'Image', 'fusion' ),
				'type' => Controls_Manager::MEDIA,
				'label_block' => true,
			]
		);

		$image_sizes = fusion_get_image_sizes();
		$this->add_control(
			'image_size',
			[
				'label' => esc_html__( 'Image Size', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'fusion-medium',
				'options' => $image_sizes
			]
		);

		$this->add_control(
			'name',
			[
				'label' => esc_html__( 'Name', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => 'John Doe',
				'default' => 'John Doe',
				'label_block' => true,
			]
		);

		$this->add_control(
			'title',
			[
				'label' => esc_html__( 'Title / Position', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => 'Founder',
				'default' => 'Founder',
				'label_block' => true,
			]
		);

		$this->add_control(
			'info_position',
			[
				'label' => esc_html__( 'Text position', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'outside',
				'options' => [
					'left' => esc_html__( 'Left', 'fusion' ),
					'bottom' => esc_html__( 'Bottom', 'fusion' ),
					'right' => esc_html__( 'Right', 'fusion' ),
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		$image = '';
		if ($settings['image'])
			$image = wp_get_attachment_image_src($settings['image']['id'], $settings['image_size'])[0];

		$params = [
			'image' => $image,
			'name' => $settings['name'],
			'title' => $settings['title'],
			'info_position' => $settings['info_position']
		];
		$team_member = \fusion_load_template_part( 'content-team-member', $params );
		print($team_member);
	}

	protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Widget_Team_Member() );
