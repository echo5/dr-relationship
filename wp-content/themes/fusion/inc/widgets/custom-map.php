<?php
namespace Elementor;

use Elementor\Widget_Base;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Map extends Widget_Base {

	public function get_name() {
		return 'custom_map';
	}

	public function get_title() {
		return esc_html__( 'Custom Map', 'fusion' );
	}

	public function get_icon() {
		return 'eicon-google-maps';
	}

	public function get_categories() {
		return [ 'fusion-widgets' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_custom_map',
			[
				'label' => esc_html__( 'Map', 'fusion' ),
			]
		);

		$default_lat = '33.7490';
		$default_long = '84.3880';
		$this->add_control(
			'lat',
			[
				'label' => esc_html__( 'Latitude', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => $default_lat,
				'default' => $default_lat,
				'label_block' => true,
			]
		);

		$this->add_control(
			'long',
			[
				'label' => esc_html__( 'Longitude', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => $default_long,
				'default' => $default_long,
				'label_block' => true,
			]
		);

		// $this->add_control(
		// 	'height',
		// 	[
		// 		'label' => esc_html__( 'Height', 'fusion' ),
		// 		'type' => Controls_Manager::SLIDER,
		// 		'default' => [
		// 			'size' => 250,
		// 		],
		// 		'range' => [
		// 			'px' => [
		// 				'min' => 100,
		// 				'max' => 600,
		// 			],
		// 		],
		// 	]
		// );


		$this->add_control(
			'marker',
			[
				'label' => esc_html__( 'Marker Image', 'fusion' ),
				'type' => Controls_Manager::MEDIA,
				'label_block' => true,
			]
		);

		$this->add_control(
			'zoom',
			[
				'label' => esc_html__( 'Zoom Level', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 10,
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 20,
					],
				],
			]
		);

		$this->add_control(
			'height',
			[
				'label' => esc_html__( 'Height', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 300,
				],
				'range' => [
					'px' => [
						'min' => 40,
						'max' => 1440,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .map-canvas' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
		  'custom_styles',
		  [
		     'label'   => esc_html__( 'Custom Styles', 'fusion' ),
		     'type'    => Controls_Manager::CODE,
		     'language' => 'json',
		     'description' => wp_kses( __( 'Insert your Javascript styling array here to customize the map\'s look.  <a href="https://snazzymaps.com/" target="_blank">https://snazzymaps.com</a>', 'fusion'), array( 'a' => array( 'href' => array(),'target' => array())) )
		  ]
		);

		// $this->add_control(
		// 	'prevent_scroll',
		// 	[
		// 		'label' => esc_html__( 'Prevent Scroll', 'fusion' ),
		// 		'type' => Controls_Manager::SWITCHER,
		// 		'default' => 'yes',
		// 		'label_on' => esc_html__( 'Yes', 'fusion' ),
		// 		'label_off' => esc_html__( 'No', 'fusion' ),
		// 		'selectors' => [
		// 			'{{WRAPPER}} iframe' => 'pointer-events: none;',
		// 		],
		// 	]
		// );

		$this->add_control(
			'view',
			[
				'label' => esc_html__( 'View', 'fusion' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		if ( empty( $settings['lat'] ) || empty( $settings['long'] ) )
			return;

		if ( 0 === absint( $settings['zoom']['size'] ) )
			$settings['zoom']['size'] = 10;

		$map_options = [
			'lat' => $settings['lat'],
			'long' => $settings['long'],
			'zoom' => $settings['zoom']['size'],
			'styles' => !empty($settings['custom_styles']) ? json_decode($settings['custom_styles']) : null,
			'marker' => $settings['marker']['url'],
		];

		ob_start();

		?>
		<div id="map-canvas" class="map-canvas" data-map-options="<?php echo esc_attr( wp_json_encode( $map_options ) ); ?>"></div>
    <?php
    $html = ob_get_clean();
    print($html);
	}

	protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Widget_Custom_Map() );
