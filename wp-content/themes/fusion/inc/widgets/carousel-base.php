<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

abstract class Widget_Carousel_Base extends Widget_Base {

	public function get_categories() {
		return [ 'fusion-widgets' ];
	}

	public function get_icon() {
		return 'eicon-slider-push';
	}

	public function get_script_depends() {
		return [ 'jquery-slick' ];
	}

	public function is_reload_preview_required() {
		return true;
	}

	protected function carousel_controls() {

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'image',
			]
		);

		$slides_to_show = range( 1, 10 );
		$slides_to_show = array_combine( $slides_to_show, $slides_to_show );

		$this->add_control(
			'slides_to_show',
			[
				'label' => esc_html__( 'Slides in View', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'options' => $slides_to_show,
			]
		);

		$this->add_control(
			'slides_to_scroll',
			[
				'label' => esc_html__( 'Slides to Scroll', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => '2',
				'options' => $slides_to_show,
				'condition' => [
					'slides_to_show!' => '1',
				],
			]
		);

		$this->add_control(
			'gap',
			[
				'label' => esc_html__( 'Gap', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 100,
					],
				],
				'default' => [
					'size' => 0,
				],
				'show_label' => true,
				'selectors' => [
					'{{WRAPPER}} .slick-list' => 'margin-left: -{{SIZE}}{{UNIT}}; margin-right: -{{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .slick-slide' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'image_stretch',
			[
				'label' => esc_html__( 'Image Stretch', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'no' => esc_html__( 'No', 'fusion' ),
					'yes' => esc_html__( 'Yes', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'stretch_carousel',
			[
				'label' => esc_html__( 'Stretch Carousel', 'fusion' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => esc_html__( 'Yes', 'fusion' ),
				'label_off' => esc_html__( 'No', 'fusion' ),
				'return_value' => 'yes',
				'selectors' => [
					'{{WRAPPER}}' => 'position: absolute; width: 100%; height: 100%;',
				],
			]
		);

		$this->add_control(
			'navigation',
			[
				'label' => esc_html__( 'Navigation', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both' => esc_html__( 'Arrows and Dots', 'fusion' ),
					'arrows' => esc_html__( 'Arrows', 'fusion' ),
					'dots' => esc_html__( 'Dots', 'fusion' ),
					'none' => esc_html__( 'None', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'show_slide_counter',
			[
				'label' => esc_html__( 'Show Slide Counter', 'fusion' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => esc_html__( 'Show', 'fusion' ),
				'label_off' => esc_html__( 'Hide', 'fusion' ),
				'return_value' => 'yes',
			]
		);


		$this->end_controls_section();


		$this->start_controls_section(
			'section_overlay',
			[
				'label' => esc_html__( 'Overlay', 'fusion' ),
			]
		);


		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background_overlay',
				'types' => [ 'none', 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} .grid-item .overlay',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => esc_html__( 'Additional Options', 'fusion' ),
			]
		);

		$this->add_control(
			'pause_on_hover',
			[
				'label' => esc_html__( 'Pause on Hover', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => esc_html__( 'Yes', 'fusion' ),
					'no' => esc_html__( 'No', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'carousel_class',
			[
				'label' => esc_html__( 'Carousel Class', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'description' => esc_html__( 'Class added to the carousel item (e.g., ".my-nav-carousel")', 'fusion' ),
			]
		);

		$this->add_control(
			'sync_with',
			[
				'label' => esc_html__( 'Sync With', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'description' => esc_html__( 'Selector of another carousel to sync with (e.g., ".my-other-nav-carousel")', 'fusion' ),
			]
		);

		$this->add_control(
			'center_mode',
			[
				'label' => esc_html__( 'Center Mode', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => esc_html__( 'Yes', 'fusion' ),
					'no' => esc_html__( 'No', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => esc_html__( 'Autoplay', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => esc_html__( 'Yes', 'fusion' ),
					'no' => esc_html__( 'No', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => esc_html__( 'Autoplay Speed', 'fusion' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label' => esc_html__( 'Infinite Loop', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => esc_html__( 'Yes', 'fusion' ),
					'no' => esc_html__( 'No', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'effect',
			[
				'label' => esc_html__( 'Effect', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'slide',
				'options' => [
					'slide' => esc_html__( 'Slide', 'fusion' ),
					'fade' => esc_html__( 'Fade', 'fusion' ),
				],
				'condition' => [
					'slides_to_show' => '1',
				],
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => esc_html__( 'Animation Speed', 'fusion' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
			]
		);

		$this->add_control(
			'direction',
			[
				'label' => esc_html__( 'Direction', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'ltr',
				'options' => [
					'ltr' => esc_html__( 'Left', 'fusion' ),
					'rtl' => esc_html__( 'Right', 'fusion' ),
				],
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => esc_html__( 'Navigation', 'fusion' ),
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label' => esc_html__( 'Arrows', 'fusion' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => esc_html__( 'Arrows Position', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'inside',
				'options' => [
					'inside' => esc_html__( 'Inside', 'fusion' ),
					'outside' => esc_html__( 'Outside', 'fusion' ),
					'outside-bottom-left' => esc_html__( 'Outside Bottom Left', 'fusion' ),
					'outside-bottom-right' => esc_html__( 'Outside Bottom Right', 'fusion' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => esc_html__( 'Arrows Size', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 20,
						'max' => 60,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .slick-slider .slick-prev:before, {{WRAPPER}} .slick-slider .slick-next:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_color',
			[
				'label' => esc_html__( 'Arrows Color', 'fusion' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .slick-slider .slick-prev:before, {{WRAPPER}} .slick-slider .slick-next:before' => 'color: {{VALUE}};',
					'{{WRAPPER}} .slick-slider .slick-prev .nav-text, {{WRAPPER}} .slick-slider .slick-next .nav-text' => 'color: {{VALUE}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => esc_html__( 'Dots', 'fusion' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_position',
			[
				'label' => esc_html__( 'Dots Position', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'outside',
				'options' => [
					'outside' => esc_html__( 'Outside', 'fusion' ),
					'inside' => esc_html__( 'Inside', 'fusion' ),
					'inside-right' => esc_html__( 'Inside Right', 'fusion' ),
					'inside-left' => esc_html__( 'Inside Left', 'fusion' ),
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		// $this->add_control(
		// 	'dots_size',
		// 	[
		// 		'label' => esc_html__( 'Dots Size', 'fusion' ),
		// 		'type' => Controls_Manager::SLIDER,
		// 		'range' => [
		// 			'px' => [
		// 				'min' => 5,
		// 				'max' => 10,
		// 			],
		// 		],
		// 		'selectors' => [
		// 			'{{WRAPPER}} .elementor-image-carousel .slick-dots li button:before' => 'font-size: {{SIZE}}{{UNIT}};',
		// 		],
		// 		'condition' => [
		// 			'navigation' => [ 'dots', 'both' ],
		// 		],
		// 	]
		// );

		$this->add_control(
			'dots_color',
			[
				'label' => esc_html__( 'Dots Color', 'fusion' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-slick-slider ul.slick-dots li button:before' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();


	}

	protected function render_carousel($slides, $settings, $post_type = '') {

		if ( $post_type) {
			$query = fusion_get_post_items_by_settings( $settings, $post_type );
			$slides = $query->posts;
		} 

		if ( empty( $slides ) ) {
			return;
		}

		$is_slideshow = '1' === $settings['slides_to_show'];
		$is_rtl = ( 'rtl' === $settings['direction'] );
		$direction = $is_rtl ? 'rtl' : 'ltr';
		$show_dots = ( in_array( $settings['navigation'], [ 'dots', 'both' ] ) );
		$show_arrows = ( in_array( $settings['navigation'], [ 'arrows', 'both' ] ) );

		$slick_options = [
			'slidesToShow' => absint( $settings['slides_to_show'] ),
			'autoplaySpeed' => absint( $settings['autoplay_speed'] ),
			'autoplay' => ( 'yes' === $settings['autoplay'] ),
			'infinite' => ( 'yes' === $settings['infinite'] ),
			'pauseOnHover' => ( 'yes' === $settings['pause_on_hover'] ),
			'centerMode' => ( 'yes' === $settings['center_mode'] ),
			'asNavFor' => $settings['sync_with'],
			'speed' => absint( $settings['speed'] ),
			'arrows' => $show_arrows,
			'dots' => $show_dots,
			'rtl' => $is_rtl,
		];

		$carousel_classes = [ 'carousel' ];
		$carousel_classes[] = $settings['carousel_class'];

		if ( $show_arrows ) {
			$carousel_classes[] = 'slick-arrows-' . $settings['arrows_position'];
		}

		if ( $show_dots ) {
			$carousel_classes[] = 'slick-dots-' . $settings['dots_position'];
		}

		if ( 'yes' === $settings['image_stretch'] ) {
			$carousel_classes[] = 'slick-image-stretch';
		}

		if ( !empty($settings['post_type_style']) ) {
			$carousel_classes[] = $settings['post_type_style'];
		}

		if ( ! $is_slideshow ) {
			$slick_options['slidesToScroll'] = absint( $settings['slides_to_scroll'] );
		} else {
			$slick_options['fade'] = ( 'fade' === $settings['effect'] );
		}
		?>
		<div class="carousel-wrapper elementor-slick-slider <?php echo ($settings['stretch_carousel'] == 'yes') ? 'fit-to-container' : ''; ?>" dir="<?php echo $direction; ?>" data-carousel-settings='<?php echo esc_attr( wp_json_encode( $slick_options ) ); ?>'>
			<div class="<?php echo implode( ' ', $carousel_classes ); ?> slick-slider">
				<?php $n = 0; ?>
				<?php if ($post_type) global $post; ?>
				<?php foreach ($slides as $post): ?>
					<div class="slick-slide">
						<div class="slick-slide-inner">
							<div class="grid-item">
							<?php 
								$GLOBALS['image_size'] = $settings['image_size'];
								if ( $post_type && is_a($post, 'WP_Post') ): 
									setup_postdata($post);
									echo \fusion_load_template_part( $post_type . '/' . 'grid' );
								else:
									echo $post;
								endif;
								$n++;
							?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<?php if ($post_type) wp_reset_postdata(); ?>
			</div>
			<?php if ($settings['show_slide_counter'] == 'yes'): ?>
				<div class="carousel-slide-counter"></div>
			<?php endif; ?>
		</div>
		<?php
	}


}
