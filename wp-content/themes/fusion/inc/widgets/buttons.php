<?php

/**
 * Button addons
 */
function fusion_add_options_to_elementor_buttons($element, $args) {

	$element->add_control(
		'style' ,
		[
			'label'        => 'Style',
			'type'         => Elementor\Controls_Manager::SELECT,
			'default'      => 'default',
			'options'      => array( 'default' => 'Default', 'primary' => 'Primary' ),
			'prefix_class' => 'btn-',
			'label_block'  => true,
		]
	);

}
add_action( 'elementor/element/button/section_button/after_section_start', 'fusion_add_options_to_elementor_buttons', 10, 2);