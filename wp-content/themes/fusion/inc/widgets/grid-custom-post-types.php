<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Post_Type_Grid extends Widget_Grid_Base {

	public function get_name() {
		return 'custom-post-type-grid';
	}

	public function get_title() {
		return esc_html__( 'Custom Post Type Grid', 'fusion' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_grid',
			[
				'label' => esc_html__( 'Grid', 'fusion' ),
			]
		);

		$args = array(
			'public'   => true,
		);
		$post_types = get_post_types( $args );
		$this->add_control(
		  'post_types',
		  [
		     'label' => esc_html__( 'Post Types', 'fusion' ),
		     'type' => Controls_Manager::SELECT2,
		     'options' => $post_types,
		     'multiple' => true,
		  ]
		);

		$post_styles = apply_filters( 'luxe_post_grid_styles', array('default' => 'Default'));
		$this->add_control(
			'post_style',
			[
				'label' => esc_html__( 'Post Style', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => $post_styles,
			]
		);
		
		$this->grid_controls();

		$this->end_controls_section();


	}

	protected function render() {
		$settings = $this->get_settings();
		$settings['post_type_style'] = 'post-style-' . $settings['post_style'];
		$GLOBALS['post_grid_style'] = $settings['post_style'];
		$this->render_grid(null, $settings, $settings['post_types']);
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Custom_Post_Type_Grid() );

