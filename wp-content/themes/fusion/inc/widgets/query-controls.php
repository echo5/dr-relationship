<?php

/**
 * Post controls
 */

function fusion_add_post_options_to_widget($element, $args) {

    $element->start_controls_section(
        'section_query',
        [
            'label' => esc_html__( 'Posts Query', 'fusion' ),
        ]
    );

    $element->add_control(
        'posts_per_page',
        [
            'label' => esc_html__( 'Posts per page', 'fusion' ),
            'type' => \Elementor\Controls_Manager::NUMBER,
            'default' => -1,
        ]
    );

    $element->add_control(
        'offset',
        [
            'label' => esc_html__( 'Offset', 'fusion' ),
            'type' => \Elementor\Controls_Manager::NUMBER,
            'default' => 0,
        ]
    );

    $element->add_control(
        'category',
        [
            'label' => esc_html__( 'Category', 'fusion' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'description' => esc_html__( 'A list of comma separated category slugs to include (e.g., latest-news,my-blog-category)', 'fusion' )
        ]
    );

    $element->add_control(
        'include',
        [
            'label' => esc_html__( 'Include Items', 'fusion' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'description' => esc_html__( 'A list of comma separated IDs of items to include (e.g., 12,36,10)', 'fusion' )
        ]
    );

    $element->add_control(
        'exclude',
        [
            'label' => esc_html__( 'Exclude Items', 'fusion' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'description' => esc_html__( 'A list of comma separated IDs of items to exclude (e.g., 12,36,10)', 'fusion')
        ]
    );

    $element->add_control(
        'order',
        [
            'label' => esc_html__( 'Order', 'fusion' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'default' => 'ASC',
            'options' => [
                'ASC' => esc_html__( 'Ascending', 'fusion' ),
                'DESC' => esc_html__( 'Descending', 'fusion' ),
            ],
        ]
    );

    $element->add_control(
        'orderby',
        [
            'label' => esc_html__( 'Order By', 'fusion' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'default' => '',
            'options' => [
                'menu_order' => esc_html__( 'Menu Order', 'fusion' ),
                'date' => esc_html__( 'Date', 'fusion' ),
                'title' => esc_html__( 'Title', 'fusion' ),
                'modified' => esc_html__( 'Date Modified', 'fusion' ),
                'rand' => esc_html__( 'Random', 'fusion' ),
            ],
        ]
    );

    $element->add_control(
        'meta_query_relation',
        [
            'label' => esc_html__( 'Meta Query Relation', 'fusion' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'default' => 'AND',
            'options' => [
                'AND' => esc_html__( 'AND', 'fusion' ),
                'OR' => esc_html__( 'OR', 'fusion' ),
            ],
        ]
    );

    $element->add_control(
        'meta_query',
        [
            'label' => esc_html__( 'Meta Query', 'fusion' ),
            'type' => \Elementor\Controls_Manager::REPEATER,
            'prevent_empty' => false,
            'default' => array(),
            'fields' => [
                [
                    'name' => 'key',
                    'label' => esc_html__( 'Key', 'fusion' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'show_label' => true,
                ],
                [
                    'name' => 'value_type',
                    'label' => esc_html__( 'Value Type', 'fusion' ),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'default' => 'string',
                    'options' => [
                        'string' => esc_html__( 'String', 'fusion' ),
                        'date' => esc_html__( 'Date', 'fusion' ),
                        'array' => esc_html__( 'Array', 'fusion' ),
                    ],
                    'description' => esc_html__( 'Use string format for default value searching (e.g., "red"), date with a date format and strtotime (e.g., "Y-m-d,next Friday"), or array with a comma separated list (e.g., "red,blue,green").', 'fusion' ),
                    'show_label' => true,
                ],
                [
                    'name' => 'value',
                    'label' => esc_html__( 'Value', 'fusion' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'show_label' => true,
                ],
                [
                    'name' => 'compare',
                    'label' => esc_html__( 'Compare', 'fusion' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'show_label' => true,
                ],
                [
                    'name' => 'type',
                    'label' => esc_html__( 'Type', 'fusion' ),
                    'default' => 'CHAR',
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'show_label' => true,
                    'description' => esc_html__( "Possible values are 'NUMERIC', 'BINARY', 'CHAR', 'DATE', 'DATETIME', 'DECIMAL', 'SIGNED', 'TIME', 'UNSIGNED'. Default value is 'CHAR'.", 'fusion')
                ],
            ],
        ]
    );

    $element->end_controls_section();

}
add_action( 'elementor/element/portfolio-grid/section_grid/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/post-grid/section_grid/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/product-grid/section_grid/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/portfolio-carousel/section_carousel/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/post-carousel/section_carousel/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/custom-post-type-grid/section_grid/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
