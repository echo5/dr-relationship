<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fusion
 */

get_header();

	if ( have_posts() ) :

		if ( is_home() && ! is_front_page() ) : ?>
		<h1 class="page-title mb-5"><?php single_post_title(); ?></h1>

		<?php
		endif;
		?>

		<div class="luxe-grid luxe-grid-masonry post-style-<?php echo fusion_get_post_grid_style(); ?> row">
			<?php
				$GLOBALS['image_size'] = 'fusion-wide';
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					?>
					<div class="grid-item col-md-3 col-sm-6">
					<?php
					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/grid', fusion_get_post_grid_style() );
					?>
					</div>
					<?php
				endwhile;
				?>
		</div>

		<?php fusion_index_navigation(); ?>

		<?php

	else :

		get_template_part( 'templates/content', 'none' );

	endif;

		?>


<?php get_footer(); ?>
