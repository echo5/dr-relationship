<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package fusion
 */

get_header(); ?>

		<div class="container">
			
			<section class="error-404 not-found">
				<h1 class="page-title text-center mb-5"><?php esc_html_e( '404', 'fusion' ); ?></h1>

				<div class="oops">
						<p><?php esc_html_e( 'Oops, we can\'t seem to find the page you\'re looking for.  Let\'s see if this can get you on the right trail.', 'fusion' ); ?></p>

						<?php
							get_search_form();
						?>
				</div>

			</section><!-- .error-404 -->
			
		</div>

<?php
get_footer();
