<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fusion
 */

get_header(); ?>

	<?php
		the_archive_title( '<h1 class="page-title mb-5">', '</h1>' );
		the_archive_description( '<div class="archive-description">', '</div>' );
	?>

	<div class="row">
		<div class="col-md-8 post-style-<?php echo fusion_get_post_grid_style(); ?>">

			<?php
			if ( have_posts() ) : ?>

				<?php
				$GLOBALS['image_size'] = 'fusion-wide';
				/* Start the Loop */
				while ( have_posts() ) : the_post();
				?>

					<div class="grid-item">
					<?php						
					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/grid', fusion_get_post_grid_style() );

					?>
					</div>
					<?php

				endwhile;

				fusion_index_navigation();

			else :

				get_template_part( 'templates/content', 'none' );

			endif; ?>

		</div>
		<div class="col-md-3 offset-md-1">
			<?php get_sidebar(); ?>
		</div>
	</div>

<?php
get_footer();
