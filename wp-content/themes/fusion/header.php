<?php
/**
 * The header
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fusion
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php echo fusion_body_atts(); ?>>

	<?php if ( is_active_sidebar( 'header-override' ) && fusion_is_elementor_active() ) { ?>
		<nav class="navbar navbar-custom navbar-toggleable-md">
			<?php dynamic_sidebar( 'header-override' ); ?>
		</nav>
	<?php } else { ?>
		<?php get_template_part('template-parts/header/' . fusion_get_header_style() ); ?>
	<?php } ?>

	<div id="barba-wrapper">
	  <div class="barba-container">
			<div id="content" class="site-content <?php echo (!fusion_is_elementor_page() && (!is_single() || fusion_is_product()) ? 'container' : ''); ?>">
