<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-1 col-main">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<div class="featured">
					<?php the_post_thumbnail('full'); ?>
				</div>

				<header class="entry-header mb-5 mt-4">
					<?php
					the_title( '<h1 class="entry-title">', '</h1>' );
					?>
					<?php get_template_part('template-parts/post/meta'); ?>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
				
				<?php fusion_page_links(); ?>
				<footer class="entry-footer">
					<div class="post-nav-container">
						<?php fusion_post_navigation(); ?>
					</div>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

			<?php
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>
		</div>
		<div class="col-lg-3 col-sidebar">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>


