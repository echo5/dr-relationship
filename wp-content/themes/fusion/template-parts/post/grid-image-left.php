<article id="post-<?php the_ID(); ?>" <?php post_class('mb-5'); ?>>
	<div class="row">
		<div class="featured col-md-5">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail($GLOBALS['image_size']); ?>
			</a>
		</div><!-- .featured -->

		<div class="col-md-7 pl-md-5">
			<div class="categories mb-2">
				<?php the_category(', '); ?>
			</div>
			<a href="<?php the_permalink(); ?>">
				<?php the_title( '<h3 class="h2 entry-title mb-4">', '</h3>' ); ?>
			</a>
			<?php the_excerpt(); ?>
			<div class="post-meta mt-5">
				<span class="author"><?php the_author(); ?></span>
				<span class="date"><?php the_date(fusion_default_date_format()); ?></span>
			</div>
		</div>
	</div>

</article><!-- #post-## -->