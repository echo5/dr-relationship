<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
	</div><!-- .featured -->

	<div class="overlay overlay-gradient">
			<div class="categories badges mb-3">
				<?php the_category(', '); ?>
			</div>
			<a href="<?php the_permalink(); ?>">
				<?php the_title( '<h3 class="h2 entry-title mb-3">', '</h3>' ); ?>
			</a>
			<div class="post-meta">
				<span class="author"><?php the_author(); ?></span>
				<span class="date"><?php the_date(fusion_default_date_format()); ?></span>
			</div>
	</div>

</article><!-- #post-## -->