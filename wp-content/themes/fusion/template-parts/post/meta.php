<div class="post-meta">
	<div class="author">
		<span class="label h5"><?php esc_html__( 'Author:', 'fusion'); ?></span> <?php the_author(); ?>
	</div>
	<div class="date">
		<span class="label h5"><?php esc_html__( 'Date:', 'fusion'); ?></span> <?php the_date(fusion_default_date_format()); ?>
	</div>
	<div class="page-header-categories">
		<span class="label h5"><?php esc_html__( 'Categories:', 'fusion'); ?></span> <?php the_category(', '); ?>
	</div>
</div>