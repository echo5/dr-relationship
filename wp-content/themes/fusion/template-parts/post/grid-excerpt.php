<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured mb-3">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
	</div><!-- .featured -->

	<a href="<?php the_permalink(); ?>">
		<?php the_title( '<h3 class="entry-title mb-4">', '</h3>' ); ?>
	</a>

	<?php the_excerpt(); ?>

	<a href="<?php the_permalink(); ?>" class="moretag h6"><?php esc_html_e('Read More', 'fusion'); ?></a>

</article><!-- #post-## -->