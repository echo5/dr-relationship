<?php
/**
 * Template part for displaying portfolio content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fusion
 */

?>
<?php get_template_part( 'template-parts/portfolio/portfolio', fusion_get_portfolio_grid_style() ); ?>
