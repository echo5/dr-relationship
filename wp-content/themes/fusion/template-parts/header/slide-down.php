<nav class="navbar navbar-toggleable-md fixed-top">
  <?php fusion_nav_button('slide'); ?>
  <?php get_template_part('template-parts/header/branding'); ?>
  <?php dynamic_sidebar( 'header-1' ); ?>
  <?php
  wp_nav_menu( array(
      'theme_location'    => 'menu-1',
      'menu_id'           => 'nav-primary',
      'depth'             => 2,
      'container'         => 'div',
      'container_class'   => 'collapse navbar-collapse',
      'container_id'      => 'navbar',
      'menu_class'        => 'nav navbar-nav',
      'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
      'walker'            => new WP_Bootstrap_Navwalker())
  );
  ?>
  <?php dynamic_sidebar( 'header-2' ); ?>
</nav>