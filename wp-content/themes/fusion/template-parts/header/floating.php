<nav class="navbar navbar-toggleable-md">
    <div class="navbar-inner">
        <?php get_template_part('template-parts/header/branding'); ?>
        <?php dynamic_sidebar( 'header-1' ); ?>
        <?php
        wp_nav_menu( array(
            'theme_location'    => 'menu-1',
            'menu_id'           => 'nav-primary',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'navbar',
            'menu_class'        => 'nav navbar-nav',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker())
        );
        ?>
        <div class="ml-auto"></div>
        <div class="col-md-auto hidden-sm-down">
            <?php dynamic_sidebar( 'header-2' ); ?>
        </div>
        <?php fusion_nav_button(); ?>
    </div>
</nav>
