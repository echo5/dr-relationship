<nav class="navbar navbar-toggleable-xs fixed-top">
  <?php get_template_part('template-parts/header/branding'); ?>
  <div class="col-md-auto">
    <?php dynamic_sidebar( 'header-1' ); ?>
  </div>
  <div class="ml-auto"></div>
  <div class="ml-auto pr-5 hidden-sm-down">
    <?php dynamic_sidebar( 'header-2' ); ?>
  </div>
  <?php fusion_nav_button('overlay'); ?>
</nav>
<?php
wp_nav_menu( array(
    'theme_location'    => 'menu-1',
    'menu_id'           => 'nav-primary',
    'depth'             => 2,
    'container'         => 'div',
    'container_class'   => 'collapse nav-overlay',
    'container_id'      => 'navbar',
    'menu_class'        => 'nav navbar-nav',
    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
    'walker'            => new WP_Bootstrap_Navwalker())
);
?>
<!-- <button class="navbar-toggler close-toggler" data-sequence="overlay">
  <span class="nav-text">Close</span> <i class="icon icon-cross2"></i>
</button> -->