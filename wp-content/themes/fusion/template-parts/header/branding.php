<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand">
	<?php if ( get_theme_mod('logo_light') ): ?>
			<img src="<?php echo get_theme_mod('logo_light'); ?>" alt="<?php bloginfo( 'name' ); ?>" class="logo-light">
	<?php else: ?>
		<span class="logo-light">
			<?php bloginfo( 'name' ); ?>	
		</span>
	<?php endif; ?>
	<?php if ( get_theme_mod('logo_dark') ): ?>
			<img src="<?php echo get_theme_mod('logo_dark'); ?>" alt="<?php bloginfo( 'name' ); ?>" class="logo-dark">
	<?php else: ?>
		<span class="logo-dark">
			<?php bloginfo( 'name' ); ?>
		</span>
	<?php endif; ?>
</a>
<?php
$description = get_bloginfo( 'description', 'display' );
if ( $description || is_customize_preview() ) : 
  ?>
  <span class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></span>
  <?php 
endif; 
?>