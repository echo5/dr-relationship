<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="featured">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
	</div><!-- .featured -->
	
	<div class="overlay">
		<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
		<div class="excerpt h4"><?php the_excerpt(); ?></div>
		<span class="date mt-3"><?php the_date(fusion_default_date_format()); ?></span>
		<a href="<?php the_permalink(); ?>" class="overlay-link"></a>
	</div>

</article><!-- #post-## -->