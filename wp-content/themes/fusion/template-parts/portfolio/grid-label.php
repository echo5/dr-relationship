<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
	</div><!-- .featured -->
	
	<div class="overlay">
			<?php the_title( '<h5 class="entry-title">', '</h5>' ); ?>
			<div class="categories">
				<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
			</div>
	</div>
	<a href="<?php echo get_the_permalink(); ?>" class="overlay-link"></a>

</article><!-- #post-## -->