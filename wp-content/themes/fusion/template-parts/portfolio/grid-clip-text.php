<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
	<?php $overlay_color = (get_post_meta(get_the_ID(), '_color') ? get_post_meta(get_the_ID(), '_color')[0] : 'rgba(0, 0, 0, 0.84)'); ?>
	<div class="overlay" style="background-image:url('<?php the_post_thumbnail_url($GLOBALS['image_size']); ?>'); background-color: <?php echo $overlay_color; ?>">
		<?php the_title( '<h3 class="entry-title h1">', '</h3>' ); ?>
		<div class="excerpt">
			<?php the_excerpt(); ?>
		</div>
		<a href="<?php echo get_the_permalink(); ?>" class="moretag h5 text-center mt-5">Discover More</a>
		<a href="<?php the_permalink(); ?>" class="overlay-link"></a>
	</div>

</article><!-- #post-## -->