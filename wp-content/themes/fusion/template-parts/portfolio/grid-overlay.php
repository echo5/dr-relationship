<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="featured">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
	</div><!-- .featured -->
	
	<div class="overlay">
		<div class="categories mb-3">
			<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
		</div>
		<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
		<div class="excerpt mt-2"><?php the_excerpt(); ?></div>
		<a href="<?php the_permalink(); ?>" class="overlay-link"></a>
	</div>
	<a href="<?php echo get_the_permalink(); ?>" class="read-more h5">Discover More</a>

</article><!-- #post-## -->