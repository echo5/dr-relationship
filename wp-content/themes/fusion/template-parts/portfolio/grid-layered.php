<?php $accent_color = (get_post_meta(get_the_ID(), '_color') ? get_post_meta(get_the_ID(), '_color')[0] : ''); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="background-image:url('<?php the_post_thumbnail_url('full'); ?>');">

	<div class="overlay"></div>
	
	<div class="container d-flex">
		<div class="main align-self-center">
			<div class="post-info has-animation" data-animation="transition.slideRightBigIn" data-animation-delay="1600">
				<div class="separator"></div>
				<a href="<?php the_permalink(); ?>"<?php the_title( '<h3 class="entry-title h1" style="color:'.$accent_color.';">', '</h3>' ); ?></a>
				<div class="categories">
					<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
				</div>
				<div class="excerpt mb-5"><?php the_excerpt(); ?></div>
				<a href="<?php the_permalink(); ?>" class="h5 mt-5 moretag" style="color:<?php echo $accent_color; ?>;">View Details</a>
			</div>
			<div class="col-md-10 offset-md-2">
				<div class="featured has-animation" data-animation="transition.fadeIn" data-animation-delay="1000">
					<?php the_post_thumbnail('fusion-wide'); ?>
				</div>
				<?php the_title( '<div class="background-title h1 has-animation" data-animation="transition.slideLeftBigIn" data-animation-delay="200" style="color:'.$accent_color.';">', '</div>' ); ?>
			</div>
		</div>
	</div>

		<!-- <a href="<?php //the_permalink(); ?>" class="overlay-link"></a> -->

</article><!-- #post-## -->