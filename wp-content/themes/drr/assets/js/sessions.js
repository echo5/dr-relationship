(function($){
    'use strict';


    var apiKey = sessionVars.apiKey;
    var session;
    var sessionId = sessionVars.sessionId;
    var token = sessionVars.token;

    console.log(sessionVars);
    
    XMLHttpRequest.prototype = Object.getPrototypeOf(new XMLHttpRequest);

    function initializeSession() {
      session = OT.initSession(apiKey, sessionId);

      // Subscribe to a newly created stream
      session.on('streamCreated', function streamCreated(event) {
        var subscriberOptions = {
          insertMode: 'append',
          width: '100%',
          height: '100%'
        };
        session.subscribe(event.stream, 'subscriber', subscriberOptions, function callback(error) {
          if (error) {
            console.error('There was an error publishing: ', error.name, error.message);
          }
        });
       
      });

      session.on('sessionDisconnected', function sessionDisconnected(event) {
        console.error('You were disconnected from the session.', event.reason);
      });

      // Connect to the session
      session.connect(token, function callback(error) {
        // If the connection is successful, initialize a publisher and publish to the session
        if (!error) {
          $('.open-chat-btn, #end-session').removeClass('hidden-xs-up'); 

          var publisherOptions = {
            insertMode: 'append',
            width: '100%',
            height: '100%'
          };
          var publisher = OT.initPublisher('publisher', publisherOptions, function initCallback(initErr) {
            if (initErr) {
              console.error('There was an error initializing the publisher: ', initErr.name, initErr.message);
              return;
            }
            session.publish(publisher, function publishCallback(publishErr) {
              if (publishErr) {
                console.error('There was an error publishing: ', publishErr.name, publishErr.message);
              }
            });
            $('#toggle-camera').click(function(){
              publisher.cycleVideo();
            });
            $('body').addClass('session-active');
          });

        } else {
          console.error('There was an error connecting to the session: ', error.name, error.message);
        }
      });

      // Receive a message and append it to the history
      var msgHistory = document.querySelector('#history');
      session.on('signal:msg', function signalCallback(event) {
        if (event.data.length) {
            var userIcon = document.createElement('div');
            userIcon.className = 'user-icon';
            var reply = document.createElement('p');
            reply.textContent = event.data;
            var msg = document.createElement('div');        
            msg.innerHTML = userIcon.outerHTML + reply.outerHTML;
            if (!$('body').hasClass('chat-open')) {
                var currentUnread = parseInt($('.open-chat-btn').attr('data-unread')) ? parseInt($('.open-chat-btn').attr('data-unread')) : 0; 
                $('.open-chat-btn').attr('data-unread', currentUnread + 1);
                $('.open-chat-btn').addClass('has-unread');            
            }
            msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
            msg.className += ' message';
            msgHistory.appendChild(msg);
            msg.scrollIntoView();
        }
      });
    }

    // Text chat
    var form = document.querySelector('form');
    var msgTxt = document.querySelector('#msgTxt');

    // Send a signal once the user enters data in the form
    form.addEventListener('submit', function submit(event) {
      event.preventDefault();

      session.signal({
        type: 'msg',
        data: msgTxt.value
      }, function signalCallback(error) {
        if (error) {
          console.error('Error sending signal:', error.name, error.message);
        } else {
          msgTxt.value = '';
        }
      });
    });


    function endSession() {
      session.disconnect();
    }

    /**
     * Actions
     */
    $('.toggle-chat').click(function(){
        $('body').toggleClass('chat-open');
        if ($('body').hasClass('chat-open')) {
            $('.open-chat-btn').removeAttr('data-unread'); 
            $('.open-chat-btn').removeClass('has-unread');            
        }
    });

    $('#toggle-fullscreen').click(function(){
        $('body').toggleClass('video-chat-fullscreen');
    });

    $('#start-session').click(function(){
      $('#rebook-session').addClass('hidden-xs-up');
      $('#start-session').addClass('hidden-xs-up');
      $('body').addClass('session-active');      
      initializeSession();
    });

    $('#end-session').click(function(){
      $('.open-chat-btn, #end-session').addClass('hidden-xs-up');
      $('#rebook-session, #start-session').removeClass('hidden-xs-up');
      $('body').removeClass('session-active');      
      session.disconnect();
    });

    $('.navbar-brand').click(function(e){
      e.preventDefault();
    });
    
})(jQuery);
