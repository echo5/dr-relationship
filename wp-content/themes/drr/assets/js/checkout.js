/**
 * If credit wallet is available, auto checkout
 */
if(jQuery('#payment_method_wpuw').is(':checked')) {
	if (jQuery('body').hasClass('auto-credit-checkout') && jQuery('.woocommerce-error').length < 1) {
		jQuery('#place_order').click();
	}
}