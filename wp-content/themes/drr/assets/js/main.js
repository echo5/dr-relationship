/**
 * Append labels
 */
jQuery('.um-page-members .um-search-filter').each(function() {
   var labelText = jQuery(this).find(':input').attr('placeholder') ? jQuery(this).find(':input').attr('placeholder') : jQuery(this).find(':input').attr('data-placeholder');
   jQuery(this).prepend('<div class="search-label">' + labelText + '</div>'); 
});

/**
 * Move user times and languages
 */
jQuery('.user-meta-left').each(function() {
    var userPhoto = jQuery(this).parent().siblings('.um-member-photo');
    jQuery(this).appendTo(userPhoto);
});
jQuery('.um-member').each(function() {
    var buttons = jQuery(this).find('.view-profile, .book-now');
    var buttonsDiv = jQuery('<div class="profile-buttons"></div>').appendTo(jQuery(this));    
    buttons.appendTo(buttonsDiv);
});

/**
 * Update current time
 */
if (jQuery('.current-time').length) {
    var run = updateCurrentTime();
    setInterval(updateCurrentTime, 60000);
}
var xmlHttp;
function srvTime(){
    try {
        //FF, Opera, Safari, Chrome
        xmlHttp = new XMLHttpRequest();
    }
    catch (err1) {
        //IE
        try {
            xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
        }
        catch (err2) {
            try {
                xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch (eerr3) {
                //AJAX not supported, use CPU time.
                alert("AJAX not supported");
            }
        }
    }
    xmlHttp.open('HEAD',window.location.href.toString(),false);
    xmlHttp.setRequestHeader("Content-Type", "text/html");
    xmlHttp.send('');
    return xmlHttp.getResponseHeader("Date");
}

function updateCurrentTime() {
    dateText = jQuery('.current-datetime').text();
    oldDate = new Date(dateText)
    var date = new Date(oldDate.getTime() + 60000);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var time = hours + ':' + minutes + ' ' + ampm;
    // var time = dateTime.getHours() + ":" + dateTime.getMinutes();
    jQuery('.current-time').html(time);
}

/**
 * Multistep registration forms
 */
jQuery('.um-287').find("input,select").keypress(function(e) {
  var key = e.charCode || e.keyCode || 0;     
  if (key == 13) {
    e.preventDefault();
  }
});
jQuery('.um-287').find('.um-row').each(function(){
   jQuery(this).append('<a href="#" class="btn btn-default um-previous-tab">Previous</a>'); 
   jQuery(this).append('<a href="#" class="btn btn-default um-next-tab">Next</a>'); 
});
jQuery('.um-287').find('.um-row:first-child').addClass('active');
jQuery('.um-287').find('.um-col-alt').appendTo('.um-row:nth-child(4)');
jQuery(document).on('click', '.um-next-tab', function(e){
    e.preventDefault();
    jQuery(this).parent().removeClass('active');
    jQuery(this).parent().next().addClass('active');
});

jQuery(document).on('click', '.um-previous-tab', function(e){
    e.preventDefault();
    jQuery(this).parent().removeClass('active');
    jQuery(this).parent().prev().addClass('active');
});
var error = jQuery('.um-287').find('.um-field-error:first');
if (error.length) {
    jQuery('.um-row').removeClass('active');
    error.closest('.um-row').addClass('active');
}

/**
 * Mobile search filter
 */
jQuery('.um-search').append('<a href="#" class="um-button toggle-search"><span>Filter Mentors</span></a>');
jQuery(document).on('click', '.toggle-search', function(e){
    e.preventDefault();
    jQuery(this).parent().toggleClass('active');
});