
/**
 * Show purchase credits overlay if not enough credits
 */
jQuery(document).on('DOMSubtreeModified', '.wc-appointments-appointment-cost', function() {
    var creditsRemaining = parseInt(jQuery('#user-credits').text());
    creditsRemaining = creditsRemaining || 0;
    var price = parseInt(jQuery('.wc-appointments-appointment-cost .woocommerce-Price-amount').text().replace ( /[^\d.]/g, '' ));
    if (price > creditsRemaining) {
        jQuery('.purchase-credits-overlay').css('display', 'flex');
        jQuery('.single_add_to_cart_button').hide();
    } else {
        jQuery('.purchase-credits-overlay').css('display', 'none');    
        jQuery('.single_add_to_cart_button').show();
    }
});