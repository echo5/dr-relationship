Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};


/* globals wc_appointment_form_params, get_querystring, jstz */
jQuery( document ).ready( function( $ ) {
	'use strict';

	var xhr;

	var wc_appointments_time_picker = {
		init: function() {
			// $( '.slot-picker' ).on( 'click', 'a', this.time_picker_init );
			$( '.slot-picker' ).on( 'click', 'a', function() {
				var form         = $(this).closest( 'form' ),
					value        = $(this).parent().data( 'slot' );

				wc_appointments_time_picker.set_selected_time( form, value );

				return false;
			});
			$( 'body' ).on( 'change', '#wc_appointments_field_staff', function() {
				wc_appointments_time_picker.show_available_time_slots( this );
			});
			$( '.wc-appointments-appointment-form' ).parents('form').on( 'date-selected', function() {
				wc_appointments_time_picker.show_available_time_slots( this );
			});
			$('<div class="requested-time-slot-tags"></div>').insertBefore('.wc-appointments-appointment-cost');
			$('.wc-appointments-appointment-form').append('<input type="hidden" class="requested-time-slots" name="requested_time_slots" />');
			$('form').on('submit', function($) {
				var selectedSlotValues = jQuery('.requested-time-slots').val().split(',');
				if (selectedSlotValues.length < 3) {
					alert('Please select at least 3 time slots.');
					return false;
				}
			});
		},
		time_picker_init: function() {
			var value  = $(this).data( 'value' ),
				target = $(this).parents( '.form-field' ).find( 'input' ),
				form   = $(this).closest( 'form' );

			// $target.val( value ).change();
			target.val( value );

            var selected = $(this).parents('.form-field').find('li.selected');
            if (selected.length < 6) {
                $(this).parents('li').addClass('selected');
            } else {
                alert('Please select a maximum of 6 timeslots or click a previously selected slot to deselect it.')
            }

			form.triggerHandler( 'time-selected' );

			return false;
		},
		set_selected_time: function( form, value ) {
			var submit_button = form.find( '.wc-appointments-appointment-form-button' );
			var slot_picker   = form.find( '.slot-picker' );

			submit_button.addClass( 'disabled' );

			if ( undefined === value ) {
				//submit_button.addClass( 'disabled' );
				form.triggerHandler( 'time-selected' );
				return;
            }

			var selected_slot = slot_picker.find( '[data-slot="' + value + '"]' ),
                selected_slot_value = selected_slot.find( 'a' ).data( 'value' );
			
			// Get current month, day, and year
			var year = form.find('.appointment_date_year').val(),
				month = form.find('.appointment_date_month').val(),
				day = form.find('.appointment_date_day').val();

            // Limit to 3 selections
			var selectedSlotValues = $('.requested-time-slots').val().length ? $('.requested-time-slots').val().trim().split(',') : [];
			var slotValue = month + '/' + day + '/' + year + ' ' + selected_slot.find('a').data('value');
			var slotDisplay = month + '/' + day + '/' + year + ' ' + selected_slot.find('a').text();			
            if (selected_slot.hasClass('selected')) {
				selected_slot.removeClass('selected');
				selectedSlotValues.remove(slotValue);								
				$('.requested-time-slot-tags').find('span[data-time-slot="' + slotValue + '"]').remove();
            }
            else if (selectedSlotValues.length < 6) {
				selected_slot.addClass( 'selected' );
				selectedSlotValues.push(slotValue);				
				$('.requested-time-slot-tags').append('<span class="time-slot" data-time-slot="' + slotValue + '">' + slotDisplay + '</span>');				
            } else {
                alert('Please select a maximum of 6 timeslots or click a previously selected slot to deselect it.');
            }

			if ( undefined === selected_slot.data( 'slot' ) ) {
				//submit_button.addClass( 'disabled' );
				form.triggerHandler( 'time-selected' );
				return;
			}

            var target = form.find( '.slot-picker' ).parent( 'div' ).find( 'input' );
            var timeSlotsInput = form.find('.requested-time-slots');

			// Add selected time value to input.
			// target.val( value ).change();
            target.val( selected_slot_value ); // timeslot in local format
            timeSlotsInput.val( selectedSlotValues ); // all time slots
			target.attr( 'data-value', value ); // timeslot in Hi format
			timeSlotsInput.change();

			form.triggerHandler( 'time-selected' );

			// slot_picker.find( 'li' ).removeClass( 'selected' );
			// selected_slot.addClass( 'selected' );
		},
		show_available_time_slots: function( element ) {
			var form            = $( element ).closest( 'form' ),
				slot_picker     = form.find( '.slot-picker' ),
				fieldset        = form.find( 'fieldset' ),
				year            = parseInt( fieldset.find( 'input.appointment_date_year' ).val(), 10 ),
				month           = parseInt( fieldset.find( 'input.appointment_date_month' ).val(), 10 ),
				day             = parseInt( fieldset.find( 'input.appointment_date_day' ).val(), 10 ),
				selected_slot   = slot_picker.find( '.selected' );

			if ( ! year || ! month || ! day ) {
				return;
			}

			// clear slots
			slot_picker.closest('div').find('input').val( '' ).change();
			slot_picker.closest('div').block({message: null, overlayCSS: {background: '#fff', backgroundSize: '16px 16px', opacity: 0.6}}).show();

			// Prevent multiple requests at once.
			// if ( xhr ) xhr.abort();

			xhr = $.ajax({
				type: 'POST',
				url: wc_appointment_form_params.ajax_url,
				data: {
					action: 'wc_appointments_get_slots',
					form: form.serialize(),
					timezone: jstz.determine().name()
				},
				success: function( code ) {
					slot_picker.html( code );
					slot_picker.closest('div').unblock();
					// wc_appointments_time_picker.set_selected_time( form, selected_slot.data( 'slot' ) );

					// Set selected from requested slots
					slot_picker.find('.selected').removeClass('selected');					
					var year = form.find('.appointment_date_year').val(),
						month = form.find('.appointment_date_month').val(),
						day = form.find('.appointment_date_day').val();
					var previouslySelected = jQuery('.requested-time-slots').val().split(',');
					$.each(previouslySelected, function(index, value) {
						if ( value.indexOf(month + '/' + day + '/' + year) !== -1 ) {
							var time = value.replace(month + '/' + day + '/' + year + ' ', '');
							time = time.replace(':', '');
							$('li[data-slot="' + time + '"]').addClass('selected');
						}
					});			

					// if time is in querystring, select it instead of the first time
					// it overrides autoselect setting
					if ( get_querystring('time') !== null && false) {
						var selected_time = slot_picker.find( 'li.slot[data-slot="' + get_querystring('time') + '"]' ).not( '.slot_empty' );

						if ( selected_time.length > 0 ) {
							selected_time.find('a').click();
						} else {
							// window.alert( wc_appointment_form_params.i18n_time_unavailable );
							wc_appointments_time_picker.autoselect_first_available_time( form );
						}
					// Auto select first available time
					} else if ( wc_appointment_form_params.is_autoselect ) {
						// wc_appointments_time_picker.autoselect_first_available_time( form );
					}
				},
				dataType: 'html'
			});
		},
		autoselect_first_available_time: function( form ) {
			var slot_picker = form.find('.slot-picker'),
				first_time  = slot_picker.find( 'li.slot:not(".slot_empty"):first' );

			if ( first_time.length > 0 && first_time.has( 'a' ) ) {
				first_time.find( 'a' ).click();
			}
		}
	};

	wc_appointments_time_picker.init();
});


// jQuery(document).on('change', '.requested-time-slots', function($) {
// 	var form = $(this).closest( 'form' );
// 	alert('test3');
// 	// Enable submit
// 	form.find('.single_add_to_cart_button').addClass('disabled');
// 	alert('disabled');
// 	if (selectedSlotValues.length == 3) {
// 		alert('is 3');
// 		form.find('.single_add_to_cart_button').removeClass('disabled');				
// 	}
// });

