<?php
/**
 * My Appointments
 *
 * Shows appointments on the account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/appointments.php.
 *
 * HOWEVER, on occasion we will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @version     1.3.1
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<?php if ( ! empty( $tables ) ) : ?>

	<?php foreach ( $tables as $table_id => $table ) : ?>

		<h2><?php echo esc_html( $table['header'] ) ?></h2>

		<table class="shop_table my_account_appointments <?php echo $table_id . '_appointments'; ?>">
			<thead>
				<tr>
					<th scope="col" class="appointment-id"><?php _e( 'ID', 'woocommerce-appointments' ); ?></th>
					<th scope="col" class="scheduled-product"><?php _e( 'Scheduled', 'woocommerce-appointments' ); ?></th>
					<th scope="col" class="order-number"><?php _e( 'Order', 'woocommerce-appointments' ); ?></th>
					<th scope="col" class="appointment-when"><?php _e( 'When', 'woocommerce-appointments' ); ?></th>
					<th scope="col" class="appointment-duration"><?php _e( 'Duration', 'woocommerce-appointments' ); ?></th>
					<th scope="col" class="appointment-status"><?php _e( 'Status', 'woocommerce-appointments' ); ?></th>
					<th scope="col" class="appointment-cancel"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $table['appointments'] as $appointment ) : ?>
					<tr>
						<td class="appointment-id"><?php echo $appointment->get_id(); ?></td>
						<td class="scheduled-product">
							<?php if ( $appointment->get_product() && $appointment->get_product()->is_type( 'appointment' ) ) : ?>
							<a href="<?php echo esc_url( get_permalink( $appointment->get_product()->get_id() ) ); ?>">
								<?php echo $appointment->get_product()->get_title(); ?>
							</a>
							<?php endif; ?>
						</td>
						<td class="order-number">
							<?php if ( $appointment->get_order() ) : ?>
							<a href="<?php echo $appointment->get_order()->get_view_order_url(); ?>">
								<?php echo $appointment->get_order()->get_order_number(); ?>
							</a>
							<?php endif; ?>
						</td>
						<td class="appointment-when"><?php echo $appointment->get_start_date(); ?></td>
						<td class="appointment-duration"><?php echo $appointment->get_duration(); ?></td>
						<td class="appointment-status"><?php echo esc_html( wc_appointments_get_status_label( $appointment->get_status() ) ); ?></td>
						<td class="appointment-session">
                            <?php if ( time() >= $appointment->get_start() - (60 * 5) && time() <= $appointment->get_end() + (60 * 60)): ?>
                                <a href="<?php echo home_url( '/session?session_id=' . $appointment->ID ); ?>" class="button">Enter Session</a>                        
                            <?php elseif ( time() >= $appointment->get_start() - (60 * 60 * 24) && time() <= $appointment->get_end() + (60 * 60) ): ?>
                                <a href="#" class="button disabled" disabled>Enter Session</a>
                            <?php endif; ?>
                        </td>
						<td class="appointment-cancel">
							<?php if ( $appointment->get_status() != 'cancelled' && $appointment->get_status() != 'completed' && ! $appointment->passed_cancel_day() ) : ?>
							<a href="<?php echo $appointment->get_cancel_url(); ?>" class="button cancel"><?php _e( 'Cancel', 'woocommerce-appointments' ); ?></a>
							<?php endif ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	<?php endforeach; ?>

<?php else : ?>
	<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php _e( 'Book', 'woocommerce-appointments' ) ?>
		</a>
		<?php _e( 'No appointments scheduled yet.', 'woocommerce-appointments' ); ?>
	</div>
<?php endif; ?>
