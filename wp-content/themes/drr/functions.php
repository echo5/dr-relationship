<?php 

define('DRR_VERSION', '2.1.0');

/** 
 * Includes
 */
require_once 'inc/helpers.php';
require_once 'inc/appointments.php';
require_once 'inc/credits.php';
require_once 'inc/referrals.php';
require_once 'inc/my-account.php';
require_once 'inc/products.php';
require_once 'inc/members.php';
require_once 'inc/sessions.php';
require_once 'inc/forms.php';
require_once 'inc/vendors.php';

/** 
 * Enqueue parent styles
 */
function drr_scripts_and_styles() {
   wp_enqueue_script('drr-main', get_stylesheet_directory_uri() .'/assets/js/main.js', array(), DRR_VERSION, true);
   wp_enqueue_style('drr-main', get_stylesheet_directory_uri() .'/style.css', array(), DRR_VERSION);
}
add_action( 'wp_enqueue_scripts', 'drr_scripts_and_styles', 1000 );

/**
 * Credits hack to put overlay beneath addons scripts
 */
function drr_credits_scripts() {
  wp_enqueue_script('drr-credits', get_stylesheet_directory_uri() .'/assets/js/credits.js', array(), DRR_VERSION, true);
}
add_action( 'woocommerce_before_add_to_cart_button', 'drr_credits_scripts', 100 );

/**
 * Add in buttons to theme options
 */
function drr_add_button_styles($buttons) {
  $buttons[] = '.et_pb_button';
  $buttons[] = 'html body .site-content .product.type-product .add_to_cart_button';
  $buttons[] = '.btn';
  return $buttons;
}
add_filter('luxe_buttons', 'drr_add_button_styles', 10, 1);

/**
 * Add GTM scripts
 */
add_action( 'wp_head', 'drr_add_gtm_head_scripts' );
add_action( 'wp_footer', 'drr_add_gtm_footer_scripts', 200 );
function drr_add_gtm_head_scripts() {
  ?>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-P42T4P2');</script>
  <!-- End Google Tag Manager -->
  <?php
}
function drr_add_gtm_footer_scripts() {
  ?>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P42T4P2"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <?php
}


?>