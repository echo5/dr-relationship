<?php
/**
 * Template Name: Session
 * 
 */

get_header(); ?>

    <?php
    global $wp_query;
    $appointment_id = apply_filters('page_session_appointment_id', $wp_query->query_vars['session_id']);
    $appointment = get_wc_appointment( $appointment_id );
    do_action('before_page_session', $appointment);
    ?>

    <h3>Session ID: <?php echo $appointment->ID; ?></h3>
    

    <!-- <iframe src="https://tokbox.com/embed/embed/ot-embed.js?embedId=a194f31b-a755-4451-8bcb-39bedae1a121&room=<?php //echo $appointment_id; ?>&iframe=true" width=1140 height=700 allow="microphone; camera" ></iframe> -->
    <div id="videos">
        <div id="toggle-fullscreen"><i class="fa fa-arrows-alt"></i></div>
        <div id="toggle-camera"><i class="fa fa-camera"></i></div>
        <div id="start-session" class="btn">Start Session</div>
        <a href="<?php echo drr_get_product_url_from_staff_id($appointment->get_staff_ids()[0]); ?>" class="btn hidden-xs-up" id="rebook-session">Rebook Session</a>
        <div id="end-session" class="btn hidden-xs-up">End Session</div>
        <div id="subscriber"></div>
        <div id="publisher"></div>
    </div>

    <div id="textchat">
            <div id="history"></div>
            <form>
                <textarea type="text" placeholder="Type your message here" id="msgTxt" rows="3"></textarea>
                <input type="submit" value="Send" class="btn btn-default">
            </form>
            <div class="close-chat-btn toggle-chat"><i class="fa fa-close"></i></div>
    </div>
    <div class="open-chat-btn toggle-chat hidden-xs-up"><i class="fa fa-comments"></i></div>

    <div class="session-meta-wrapper">
        <span class="session-meta">Start Time: <?php echo drr_get_user_time(get_current_user_id(), $appointment->get_start_date()); ?></span>
        <span class="session-meta">End Time: <?php echo drr_get_user_time(get_current_user_id(), $appointment->get_end_date()); ?></span>
        <span class="session-meta">Current Time: <span class="current-datetime"><?php echo drr_get_user_time(get_current_user_id(), 'now', 'F j, Y'); ?> <span class="current-time"><?php echo drr_get_user_time(get_current_user_id(), 'now', 'g:i a'); ?></span> </span>  </span>
    </div>
     
 <?php
 get_footer();

?>
