<?php

/**
 * User time by time zone
 */
function drr_get_user_current_time($user, $format = 'H:i') {
    $time_zone = um_user('time_zone') ? um_user('time_zone') : 'US/Eastern';
    if (!empty($time_zone)) {
      $date = new DateTime("now", new DateTimeZone($time_zone) );
      return $date->format($format);
    }
}

/**
 * Return a time in a user's local time
 */
function drr_get_user_time($user_id, $time, $format = 'D F j g:ia (\G\M\T P) e') {
    um_fetch_user($user_id);
    $time_zone = um_user('time_zone') ? um_user('time_zone') : 'US/Eastern';
    $datetime = new DateTime($time);
    $date_time_zone = new DateTimeZone($time_zone);
    if (!empty($date_time_zone)) {
        $datetime->setTimezone($date_time_zone);
    }
    return $datetime->format($format);
}

/**
 * Get user timezone string
 */
function drr_get_user_timezone($user_id) {
    um_fetch_user($user_id);    
    return um_user('time_zone') ? um_user('time_zone') : 'US/Eastern';
}

/**
 * Format time to UTC
 */
function drr_user_time_to_utc($user_id, $time, $format = 'm/d/Y H:i') {
    $date = new DateTime($time, new DateTimeZone(drr_get_user_timezone($user_id)) );
    $date->setTimezone(new DateTimeZone('UTC'));
    return $date->format($format);
}

/** 
 * Get offset between staff and customer
 */
function drr_get_timeset_offset($staff_id, $customer_id) {
    $staff_timezone = new DateTimeZone(drr_get_user_timezone($staff_id));
    $customer_timezone = new DateTimeZone(drr_get_user_timezone($customer_id));
    $staff_offset = $staff_timezone->getOffset(new DateTime('now'));
    $customer_offset = $customer_timezone->getOffset(new DateTime('now'));
    $offset = $staff_offset - $customer_offset;
    return $offset;
}