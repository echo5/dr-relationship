<?php

/**
 * Add WC email styling to Ninja Forms
 */
function drr_add_ninja_forms_email_style($message, $data, $action_settings) {
    if( 'html' == $action_settings[ 'email_format' ] ) {
        include_once WC_ABSPATH . 'includes/emails/class-wc-email.php';
        include_once WC_ABSPATH . 'includes/libraries/class-emogrifier.php';
        $wc_email = new WC_Email();
        $email_heading = '';
        ob_start();
        wc_get_template( 'emails/email-header.php', array( 'email_heading' => $email_heading ) );
        echo $message;
        wc_get_template( 'emails/email-footer.php' );
        $content = ob_get_clean();
        $message = $wc_email->style_inline( $content );
    }
    return $message;
}
add_filter( 'ninja_forms_action_email_message', 'drr_add_ninja_forms_email_style', 10, 3 );