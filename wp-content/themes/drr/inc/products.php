<?php

/**
 * Add profile image to Woocommerce page
 */
function drr_add_counselor_profile_image_product_summary() {
    $vendor = get_wcmp_product_vendors( get_the_ID() );
    echo '<div class="counselor-info">';
    echo get_avatar( $vendor->id, 200 );
    drr_time_and_language();
    echo '<a class="btn btn-secondary back-to-profile" href="' . um_user_profile_url($vendor->id) . '">View Profile</a>';    
    echo '</div>';    
}
add_action('woocommerce_before_single_product_summary', 'drr_add_counselor_profile_image_product_summary', 20);
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

/**
 * Get the product permalink from a staff id
 */
function drr_get_product_url_from_staff_id($staff_id) {
    $product_ids = WC_Data_Store::load( 'product-appointment' )->get_appointable_product_ids_for_staff( $staff_id );
    if (count($product_ids)) {
        $url = get_permalink($product_ids[0]);
        return $url;
    }
}

/**
 * Remove checkout fields
 */
function drr_remove_extra_checkout_fields( $fields ) {
    unset($fields['order']['order_comments']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
    unset($fields['order']['order_comments']);
    // unset($fields['billing']['billing_email']);
    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'drr_remove_extra_checkout_fields' );