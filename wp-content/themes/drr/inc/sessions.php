<?php

/**
 * Include OpenTok SDK
 */
require(get_stylesheet_directory() . '/vendor/autoload.php');
use OpenTok\OpenTok;

/** 
 * Enqueue parent styles
 */
function drr_session_scripts() {
    if ( get_query_var('session_id') || isset( $_GET['emergency_session'] ) ) {
        wp_enqueue_script('drr-opentok', 'https://static.opentok.com/v2/js/opentok.min.js', array(), DRR_VERSION);
        wp_enqueue_script('drr-session', get_stylesheet_directory_uri() .'/assets/js/sessions.js', array(), DRR_VERSION, true);

        // Create tokbox session
        $session_vars = drr_get_tokbox_session_vars($_GET['session_id']);
        wp_localize_script( 'drr-session', 'sessionVars', $session_vars );
    }
 }
 add_action( 'wp_enqueue_scripts', 'drr_session_scripts', 1001 );

 /**
  * Redirect if not logged in
  */
function drr_session_login_redirect()
{
    if( is_page( 'session' ) && ! is_user_logged_in() )
    {
        wp_redirect( drr_login_url() );
        die;
    }
}
add_action( 'template_redirect', 'drr_session_login_redirect' );

/** 
 * Redirect to login with current page as referring page
 */
function drr_login_url() {
    $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    return home_url( '/login?redirect_to=' . urlencode($url) );
}

/**
 * Create or find existing session info
 */
function drr_get_tokbox_session_vars($post_id) {
    $opentok = new OpenTok('46044862','ec45cc00bab283988a03c7ef8c719629dc9f7c96');
    $session_id = get_post_meta($post_id, 'tokbox_session_id', true);
    if (!$session_id) {
        // $session_id = $_GET['session_id'];
        $session = $opentok->createSession();
        $session_id = $session->getSessionId();
        update_post_meta($post_id, 'tokbox_session_id', $session_id);        
    }
    $token = $opentok->generateToken($session_id);
    
    return array(
        'apiKey' => '46044862',
        'sessionId' => $session_id,
        'token' => $token,
    );
};

/**
 * Get tokbox token from tokbox session id
 */
function drr_get_tokbox_token($session_id) {
    return $token;
};


/**
 * Session rewrite rules
 */
function drr_session_rewrites(){
    add_rewrite_rule('session', 'index.php?pagename=session&session_id=$matches[1]', 'top');
}
add_action( 'init', 'drr_session_rewrites' );
function drr_session_query_vars( $query_vars ){
    $query_vars[] = 'session_id';
    return $query_vars;
}
add_filter( 'query_vars', 'drr_session_query_vars' );


/**
 * Check appointment
 */
function drr_create_emergency_session($session_id) {
    if (isset($_GET['emergency_session'])) {
        $user = wp_get_current_user();
        if (in_array('dc_vendor', $user->roles, true)) {
            $args = array(
                'meta_query'        => array(
                    array(
                        'key'       => '_appointment_end',
                        'value'     => date( 'YmdHi' ),
                        'compare'   => '>='
                    ),
                    array(
                        'key'       => '_appointment_start',
                        'value'     => date( 'YmdHi', strtotime('-2 hours') ),
                        'compare'   => '>='
                    ),
                ),
                'author'            => $user->ID,
                'post_type'         => 'wc_appointment',
                'posts_per_page'    => '1',
            );
            $get_posts = new WP_Query();
            $posts = $get_posts->query( $args );
            
            if (count($posts)) {
                return $posts[0]->ID;
            }

            $session_id = wp_insert_post( array(
                'post_date'     => date( 'Y-m-d H:i:s' ),
                'post_date_gmt' => get_gmt_from_date( date( 'Y-m-d H:i:s' ) ),
                'post_type'     => 'wc_appointment',
                'post_status'   => 'confirmed',
                'post_author'   => $user->ID,
                'post_title'    => sprintf( __( 'Emergency Appointment &ndash; %s', 'woocommerce-appointments' ), strftime( _x( '%b %d, %Y @ %I:%M %p', 'Appointment date parsed by strftime', 'woocommerce-appointments' ) ) ),
                'ping_status'   => 'closed',
                ) 
            );
            $start = date( 'YmdHi' );
            $end = date( 'YmdHi', strtotime('+2 hours', strtotime(date("m/d/Y G:i"))) );
            update_post_meta($session_id, '_appointment_staff_id', $user->ID);
            update_post_meta($session_id, '_appointment_start', $start);
            update_post_meta($session_id, '_appointment_end', $end);

            $to = get_option('admin_email');
            $subject = 'New Emergency Session';
            $body = 'A new emergency session was created. You can view the appointment here: ' . get_edit_post_link( $session_id );;
            $headers = array('Content-Type: text/html; charset=UTF-8');

            wp_mail( $to, $subject, $body, $headers );
                
        }
    }    
    return $session_id;
}
add_filter('page_session_appointment_id', 'drr_create_emergency_session', 10, 1);

/**
 * Check appointment
 */
function drr_check_appointment_validity($appointment) {

    // None found
    if (!$appointment) {
        wp_die("Appointment not found.");
    }

    // Not paid or cancelled
    if ($appointment->get_status() != 'confirmed' && $appointment->get_status() != 'paid') {
        wp_die("This appointment has not been confirmed.");
    }

    // Not assigned staff or customer
    if (!in_array(get_current_user_id(), $appointment->get_staff_ids()) && get_current_user_id() != $appointment->get_customer_id() && $appointment->customer_id != 0 ) {
        wp_die("You do not have permission to access this appointment.");
    }

    // Appointment not started yet
    if ( time() <= $appointment->get_start() - (60 * 10) ) {
        wp_die("This video session will not be available until " . $appointment->get_start_date() . ".");
    } elseif ( time() >= $appointment->get_end() + (60 * 60) ) {
        wp_die("This video session has already expired on " . $appointment->get_end_date() . ".");
    }
}
add_action('before_page_session', 'drr_check_appointment_validity', 20);

/**
 * Add appointment avatar styles
 */
function drr_add_appointment_avatar_styles($appointment) {
    $customer_id = $appointment->get_customer_id();
    um_fetch_user($customer_id);
    $customer_avatar = um_get_avatar_uri( um_profile('profile_photo'), 60 );
    $staff_id = $appointment->get_staff_ids()[0];
    um_fetch_user($staff_id);    
    $staff_avatar = um_get_avatar_uri( um_profile('profile_photo'), 60 );
    $default = um_get_default_avatar_uri();
    if (get_current_user_id() == $customer_id) {
        $mine = $customer_avatar;
        $theirs = $staff_avatar;
    } else {
        $mine = $staff_avatar;
        $theirs = $customer_avatar;
    }
    ?>
    
    <style>
        #textchat .theirs .user-icon{
            background-image: url('<?php echo $theirs ? $theirs : $default; ?>');
        }
        #textchat .mine .user-icon{
            background-image: url('<?php echo $mine ? $mine : $default; ?>');
        }
    </style>
    <?php
}
add_action('before_page_session', 'drr_add_appointment_avatar_styles', 30);

/**
 * Add appointment avatar styles
 */
function drr_add_emergency_session_notice($appointment) {
    $user = wp_get_current_user();
    if($appointment->customer_id == 0 && in_array('dc_vendor', $user->roles, true)):
        ?>
        <div class="alert alert-info">You can share this link with anyone to allow them to join this session.  This session will expire in 2 hours.<br/>
        <?php echo home_url('/session?session_id=' . $appointment->ID); ?>
        </div>
        <?php
    endif;
}
add_action('before_page_session', 'drr_add_emergency_session_notice', 30);
