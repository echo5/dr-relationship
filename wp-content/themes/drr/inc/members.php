<?php

/**
 * Get bookable product link
 */
function drr_add_bookable_to_profile() {
    if (um_is_myprofile()):
        $myaccount_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
        ?>
        <script>
            jQuery('.um-profile-photo-img').click(function(e){
                e.preventDefault();
                window.location = '<?php echo $myaccount_url; ?>';
            });
        </script>        
        <?php
    endif;
    drr_time_and_language();
    $product_ids = WC_Data_Store::load( 'product-appointment' )->get_appointable_product_ids_for_staff( um_profile_id() );
    if (count($product_ids) && !isset($_GET['um_action'])) {
        echo '<a href="' . get_permalink($product_ids[0]) . '" class="btn book-now">Book This Mentor</a>';
    }
};
add_action('um_after_header_meta', 'drr_add_bookable_to_profile', 20);

/**
 * Move member title above profile data
 */
function drr_move_member_title() {
    echo '<h1>' . um_user('display_name') . '</h1>';
};
add_action('um_profile_content_main', 'drr_move_member_title', 5);

/**
 * Search form for members
 */
function drr_search_members($atts) {
    extract(shortcode_atts(array(
        'id' => '',
        'action' => ''
    ), $atts));

    global $ultimatemember;
    $args = $ultimatemember->access->get_meta( $id );
    foreach( $args as $k => $v ) {
        if ( $ultimatemember->validation->is_serialized( $args[$k] ) ) {
            if ( !empty( $args[$k] ) ) {
                $args[$k] = unserialize( $args[$k] );
            }
        }
    }
    ?>
    <div class="drr-search-form">
        <?php um_members_directory_search($args); ?>
    </div>
    <script>
        jQuery('.drr-search-form').find('form').attr('action', '<?php echo get_permalink($action);  ?>');
    </script>
    <?php
}
add_shortcode('search_form', 'drr_search_members');

/**
 * Add time and language beneath user avatar
 */
function drr_time_and_language() {
    global $ultimatemember;
    $languages = um_user('languages');
    ?>
    <div class="user-meta-left">
        <div>Time now: <?php echo drr_get_user_current_time($ultimatemember->user); ?></div>
        <?php if (is_array($languages)): ?>
            <div>Languages: <?php echo implode(', ', $languages); ?></div>
        <?php endif; ?>
    </div>
    <?php
}
function drr_add_time_and_language_to_grid($id) {
    drr_time_and_language();
    ?>
    <a href="<?php echo um_user_profile_url(); ?>" class="btn btn-default btn-sm view-profile">View Profile</a>
    <?php
    $product_ids = WC_Data_Store::load( 'product-appointment' )->get_appointable_product_ids_for_staff( $id );
    if (count($product_ids) && !isset($_GET['um_action'])) {
        echo '<a href="' . get_permalink($product_ids[0]) . '" class="btn book-now btn-sm">Book This Mentor</a>';
    }
}
add_action('um_members_just_after_name', 'drr_add_time_and_language_to_grid', 10, 1);

/**
 * Separate methods and areas of interest by line breaks
 */
function drr_filter_methods_and_areas_of_interest($value, $key) {
    $values = explode(',', $value);
    if (count($values) > 1) {
        $value = "<p>" . implode('</p><p>', $values);
    }
    return $value;
}
add_filter('um_profile_field_filter_hook__Methods', 'drr_filter_methods_and_areas_of_interest', 10, 2 );
add_filter('um_profile_field_filter_hook__interested-area', 'drr_filter_methods_and_areas_of_interest', 10, 2 );

/**
 * Remove edit profile link
 */
function drr_remove_edit_profile_link($items) {
    if (!current_user_can('administrator')) {
        unset($items['editprofile']);
    }
    return $items;
}
add_filter('um_myprofile_edit_menu_items', 'drr_remove_edit_profile_link', 10, 1);

/**
* Add edit profile link for admins
*/
function drr_add_edit_profile_for_admins($items) {
    if (current_user_can('administrator')) {
        $items['editprofile'] = '<a href="'.um_edit_profile_url().'" class="real_url">'.__('Edit Profile','ultimate-member').'</a>';
    }
    return $items;
}
add_filter('um_profile_edit_menu_items', 'drr_add_edit_profile_for_admins', 10, 1);

/**
 * Add redirect param to register button
 */
function drr_add_redirect_to_register_button($url, $args) {
    if (isset($_GET['redirect_to'])) {
        $url = $url . '?redirect_to=' . urlencode($_GET['redirect_to']);
    }
    return $url;
}
add_filter('um_login_form_button_two_url', 'drr_add_redirect_to_register_button', 10, 2 );

/**
 * Remove dashboard link
 */
function drr_remove_dashboard_link($url) {
    return '';
}
add_filter('wcmp_vendor_goto_dashboard', 'drr_remove_dashboard_link', 10, 1);

/**
 * Change search text
 */
function drr_um_search_text( $translated_text, $text, $domain ) {
    if ($translated_text == 'Search' && $domain == 'ultimate-member') {
        $translated_text = __( 'Search Mentors', 'drr' );
    }
	return $translated_text;
}
add_filter( 'gettext', 'drr_um_search_text', 20, 3 );

/**
 * Change email style to include Woocommerce styling
 */
function drr_um_email_content( $content, $slug, $args ) {
    include_once WC_ABSPATH . 'includes/emails/class-wc-email.php';
    include_once WC_ABSPATH . 'includes/libraries/class-emogrifier.php';
    $wc_email = new WC_Email();
    $email_heading = '';
    ob_start();
    wc_get_template( 'emails/email-header.php', array( 'email_heading' => $email_heading ) );
    echo $content;
    wc_get_template( 'emails/email-footer.php' );
    $content = ob_get_clean();
    $content = um_convert_tags( $content );
    $message = $wc_email->style_inline( $content );
    return $message;
}
add_filter( 'um_email_send_message_content', 'drr_um_email_content', 10, 3 );

/**
 * Save custom Ultimate Member fields globally
 *
 * @param array $field_args
 * @return array
 */
function drr_globally_save_custom_um_fields( $field_args ) {
    unset( $field_args['in_row'] );
    unset( $field_args['in_sub_row'] );
    unset( $field_args['in_column'] );
    unset( $field_args['in_group'] );
    unset( $field_args['position'] );
    $forms = get_posts( array (
        'posts_per_page'   => -1,
        'post_type'        => 'um_form',
    ) );        
    foreach ( $forms as $form ) {
        $custom_fields = get_post_meta( $form->ID, '_um_custom_fields', true );
        if ( array_key_exists($field_args['metakey'], $custom_fields) ) {
            UM()->fields()->update_field( $field_args['metakey'], $field_args, $form->ID );
        }
    }
    return $field_args;
}
add_filter( 'um_admin_pre_save_field_to_form', 'drr_globally_save_custom_um_fields', 10, 1 );
