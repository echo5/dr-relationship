<?php

/**
 * Only allow 1 item per cart
 */
function drr_prevent_multiple_cart_items( $cart_item_data ) {
    WC()->cart->empty_cart();
    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'drr_prevent_multiple_cart_items' );


/**
 * Go to checkout on add to cart
 */
function drr_redirect_cart_to_checkout() {
    return wc_get_checkout_url();
}
add_filter ('woocommerce_add_to_cart_redirect', 'drr_redirect_cart_to_checkout');
function redirect_to_checkout_if_cart() {
	if ( !is_cart() ) return;
	global $woocommerce;
	wp_redirect( wc_get_checkout_url(), '301' );
	exit;
}
add_action( 'template_redirect', 'redirect_to_checkout_if_cart' );

/** 
 * Remove added to cart message
 */
function tnh_remove_add_to_cart_message() {
    return;
}
add_filter( 'wc_add_to_cart_message', 'tnh_remove_add_to_cart_message' );

/** 
 * Remove currency symbol for credit based orders
 */
function drr_change_order_currency_symbol($currency_symbol, $currency) {
    if (is_wc_endpoint_url( 'view-order' ) || is_wc_endpoint_url( 'order-received' )) {
        $url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $template_name = strpos($url,'/order-received/') === false ? '/view-order/' : '/order-received/';
        if (strpos($url,$template_name) !== false) {
            $start = strpos($url,$template_name);
            $first_part = substr($url, $start+strlen($template_name));
            $order_id = substr($first_part, 0, strpos($first_part, '/'));
            $order = new WC_Order($order_id);
            if ($order->get_payment_method() == 'wpuw') {
                return false;
            }
        }
    }
    return $currency_symbol;
}
add_filter( 'woocommerce_currency_symbol', 'drr_change_order_currency_symbol', 10, 2 );



/** 
 * Disable payment methods for credits or appointments
 */
function drr_unsetting_payment_gateways( $available_gateways ) {
    if (!is_admin()) {
        foreach(WC()->cart->get_cart() as $cart_item){
           $terms = wp_get_post_terms( $cart_item['product_id'], 'product_cat' );
           $categories = array();
           foreach ( $terms as $term ) $categories[] = $term->slug;
            if (in_array( 'credit', $categories )){
                unset($available_gateways['wpuw']);
            }
            else {
                foreach($available_gateways as $key => $gateway) {
                    if ($key != 'wpuw')
                        unset($available_gateways[$key]);
                }
            }
        }
    }

    return $available_gateways;
}
add_filter('woocommerce_available_payment_gateways', 'drr_unsetting_payment_gateways', 10, 1);

/** 
 * Change timepicker
 */
function drr_enqueue_checkout_scripts() {
    if (is_checkout()){
        wp_enqueue_script('drr-checkout', get_stylesheet_directory_uri() .'/assets/js/checkout.js', array(), false, true);
    }
}
add_action('wp_footer', 'drr_enqueue_checkout_scripts');

/**
 * Add credits to user
 */
function drr_add_user_credits($user_id, $amount) {
    $current_balance = get_user_meta($user_id, '_uw_balance', true);
    $new_balance = intval($current_balance) + intval($amount);
    update_user_meta($user_id, '_uw_balance', $new_balance);
}

/** 
 * Refund credits on WC refund
 */
function drr_refund_credits_on_wc_refund($order_id, $refund_id) {
    $refund = new WC_Order_Refund($refund_id);
    $order = new WC_Order($order_id);
    if ($order->get_payment_method() == 'wpuw') {
        $user_id = get_post_meta($order_id, '_customer_user', true);    
        drr_add_user_credits($user_id, $refund->get_refund_amount());
    }
}
add_action('woocommerce_order_refunded', 'drr_refund_credits_on_wc_refund', 10, 2);

/**
 * Change button text
 */
function drr_cart_button_text() {
    return __( 'Buy Now', 'woocommerce' );
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'drr_cart_button_text' ); 
add_filter( 'woocommerce_product_add_to_cart_text', 'drr_cart_button_text' ); 

/** 
 * Redirec to checkout on 'add'
 */
function drr_add_to_cart_redirect() {
    global $woocommerce;
    $checkout_url = wc_get_checkout_url();
    return $checkout_url;
}
add_filter('woocommerce_add_to_cart_redirect', 'drr_add_to_cart_redirect');
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

// $price = apply_filters( 'woocommerce_product_addons_option_price',
// $option['price'] > 0 ? '<span class="amount-symbol">+</span>' . wc_price( get_product_addon_price_for_display( $option['price'] ) ) : '',
// $option,
// $i,
// $addon,
// 'select'
// );
function drr_change_addons_to_credit_total($string, $option, $i, $addon, $type) {
    return str_replace('+', '', $string);
}
add_filter('woocommerce_product_addons_option_price', 'drr_change_addons_to_credit_total', 10, 5);


/**
 * Auto checkout for credits only
 */
function drr_add_auto_checkout_body_classes($classes) {
    if (is_checkout()) {
        $available_gateways = WC()->payment_gateways->get_available_payment_gateways();
        foreach($available_gateways as $gateway) {
            $wpuw_active = $gateway->id == 'wpuw' ? true : false;
            break;
        }
        if (count($available_gateways) == 1 && $wpuw_active) {
            $user_credits = get_user_meta(get_current_user_id(), '_uw_balance', true);
            global $woocommerce;
            if ( $user_credits > $woocommerce->cart->total ) {
                $classes[] = 'auto-credit-checkout';
            }
        }
    }
    return $classes;
}
add_filter('body_class', 'drr_add_auto_checkout_body_classes');

/**
 * Set counselor URL in case deposit is made
 */
function drr_set_counselor_after_deposit_url() {
    if (!session_id()) {
        session_start();
    }
    if (is_product()) {
        $_SESSION['rm_mentor_redirect'] = get_permalink();
    }
}
add_action('wp', 'drr_set_counselor_after_deposit_url');

/**
 * Redirect to counselor after deposit
 */
function drr_redirect_to_counselor_after_deposit( $order_id ){
   $order = new WC_Order( $order_id );
   if ( $order->get_payment_method() != 'wpuw' && isset($_SESSION['rm_mentor_redirect']) ) {
       wp_redirect($_SESSION['rm_mentor_redirect']);
       exit;
   }
}
add_action( 'woocommerce_thankyou', 'drr_redirect_to_counselor_after_deposit');