<?php

/** 
 * Change timepicker
 */
function drr_change_time_picker() {
    if (is_product()) {
        wp_deregister_script( 'wc-appointments-time-picker' ); 
        wp_deregister_script( 'wc-appointments-date-picker' );         
        wp_enqueue_script('drr-time-picker', get_stylesheet_directory_uri() .'/assets/js/time-picker.js');    
        wp_enqueue_script('drr-date-picker', get_stylesheet_directory_uri() .'/assets/js/date-picker.js', array( 'wc-appointments-appointment-form', 'jquery-ui-datepicker', 'underscore' ));    
    }
}
add_action('wp_footer', 'drr_change_time_picker');

/** 
 * Require 3-6 time slots
 */
function drr_time_slot_validation( $passed_validation, $product_id ) { 
    $product = wc_get_product( $product_id );
    if( $product->is_type( 'appointment' ) ) {
        $time_slots = explode(',', $_POST['requested_time_slots']);
        if ( count($time_slots) < 3 ){
            wc_add_notice( __( 'Please select at least 3 time slots.', 'woocommerce' ), 'error' );
            return false; 
        }
        if ( count($time_slots) > 6 ){
            wc_add_notice( __( 'Please select at maximum of 6 time slots.', 'woocommerce' ), 'error' );
            return false; 
        }
    }

    return $passed_validation;
}
add_filter( 'woocommerce_add_to_cart_validation', 'drr_time_slot_validation', 10, 2 );

/**
 * Show selected times in cart
 */
function filter_woocommerce_add_cart_item_data( $cart_item_data, $product_id, $variation_id ) { 
    // Convert to UTC
    $time_slots = explode(',', $_POST['requested_time_slots']);
    foreach ($time_slots as $key => $slot) {
        $time_slots[$key] = drr_user_time_to_utc(get_current_user_id(), $slot);
    }
    $time_slots = implode(',', $time_slots);
    $cart_item_data['appointment']['time'] = $time_slots;
    return $cart_item_data; 
}; 
add_filter( 'woocommerce_add_cart_item_data', 'filter_woocommerce_add_cart_item_data', 11, 3 ); 

/** 
 * Add postmeta to appointments after booking
 */
function drr_order_item_meta( $item_id, $values ) {
    if ( ! empty( $values['appointment'] ) ) {
        $product        	= $values['data'];
        $appointment_id     = $values['appointment']['_appointment_id'];
    }
    
    if ( isset( $appointment_id ) ) {
        wc_add_order_item_meta( $item_id, 'requested_time_slots', $values['appointment']['time'] , false );
        $appointment        = get_wc_appointment( $appointment_id );
        
        update_post_meta( $appointment->get_id(), 'duration', $values['appointment']['duration'] );
        update_post_meta( $appointment->get_id(), 'requested_time_slots', $values['appointment']['time'] );
        $appointment_status = 'pending-confirmation';
        $appointment->set_status( $appointment_status );
        $appointment->save();
    }
}
add_action( 'woocommerce_add_order_item_meta', 'drr_order_item_meta', 51, 2 );

/** 
 * Remove fake appointment request gateway
 */
$checkout_manager = $GLOBALS['wc_appointments_checkout_manager'];
remove_filter( 'woocommerce_available_payment_gateways', array($checkout_manager, 'remove_payment_methods') );
add_filter( 'woocommerce_available_payment_gateways', 'drr_remove_payment_methods' );
function drr_remove_payment_methods( $available_gateways ) {
    if ( wc_appointment_cart_requires_confirmation() ) {
        unset( $available_gateways['wc-appointment-gateway'] );
    }
    return $available_gateways;
}

/**
 * Get appointment time slots
 */
function drr_get_appointment_time_slots($appointment_id) {
    $time_slots = get_post_meta($appointment_id, 'requested_time_slots', true); 
    $time_slots = explode(',', $time_slots);
    $appointment = get_wc_appointment($appointment_id);
    $staff_id = $appointment->get_staff_ids()[0];
    foreach ($time_slots as $key => $time_slot) {
        $time_slots[$key] = drr_get_user_time($staff_id, $time_slot);
    }
    return $time_slots;
}

/** 
 * Confirm times in admin panel
 */
function drr_admin_mark_appointment_confirmed($hook_suffix) {
    if ( !isset($_GET['appointment_slot']) || get_post_type() != 'wc_appointment' )
        return;
    drr_confirm_appointment_slot(get_the_ID(), $_GET['appointment_slot']);
}
add_action('admin_enqueue_scripts', 'drr_admin_mark_appointment_confirmed');

/** 
 * Confirm times on front-end
 */
function drr_frontend_mark_appointment_confirmed($hook_suffix) {
    if (!is_account_page() || !isset($_GET['appointment_id']) || !isset($_GET['appointment_slot']))
        return;
    drr_confirm_appointment_slot($_GET['appointment_id'], $_GET['appointment_slot']);
}
add_action('wp_head', 'drr_frontend_mark_appointment_confirmed');

/** 
 * Confirm an appointment slot
 */
function drr_confirm_appointment_slot($appointment_id, $slot) {
    
    if (! is_user_logged_in() ) {
        global $wp;
        // $redirect_url = home_url( $wp->request );
        $redirect_url = home_url($_SERVER['REQUEST_URI']);
        wp_redirect(add_query_arg( 'redirect_to', urlencode($redirect_url), get_permalink( get_option('woocommerce_myaccount_page_id') ) ) );
        exit;
    }

    $appointment = get_wc_appointment( $appointment_id );
    if ( ! $appointment ) {
        wp_die( __( 'Appointment not found', 'woocommerce-appointments' ) );
    }

    if ( ! current_user_can( 'administrator' ) && !in_array(get_current_user_id(), $appointment->get_staff_ids()) ) {
        wp_die( __( 'You do not have sufficient permissions to access this page.', 'woocommerce-appointments' ) );
    }

    $time_slots = explode(',',get_post_meta($appointment_id, 'requested_time_slots', true));
    if (! isset($time_slots[$slot]) ) {
        wp_die( __( 'Appointment slot not found.', 'woocommerce-appointments' ) );        
    }

    $duration = get_post_meta($appointment_id, 'duration', true);
    $url = wp_get_referer();
    if ( 'confirmed' !== $appointment->get_status() ) {

        $start = strtotime($time_slots[$slot]);
        $end = strtotime('+' . $duration, strtotime($time_slots[$slot]));

        $appointment->set_start( $start );        
        $appointment->set_end( $end ); 
        $appointment->update_status( 'confirmed' );        
        $url = add_query_arg( 'wc-appointment-confirmation-status', 'confirmed', $url );
    }

    wp_safe_redirect( $url );
}

/**
 * Cancel an appointment
 */
function drr_cancel_appointment($appointment_id) {

    $appointment = get_wc_appointment( $appointment_id );

    if ( ! current_user_can( 'administrator' ) && !in_array(get_current_user_id(), $appointment->get_staff_ids()) ) {
        wp_die( __( 'You do not have sufficient permissions to access this page.', 'woocommerce-appointments' ) );
    }

    $url = wp_get_referer();
    if ( 'cancelled' !== $appointment->get_status() ) {
        drr_refund_appointment($appointment);   
        $appointment->update_status( 'cancelled' );
        $url = add_query_arg( 'wc-appointment-confirmation-status', 'cancelled', $url );
    }
    
    wp_safe_redirect( $url );
}

/**
 * Cancel appointment from admin
 */
function drr_admin_cancel_appointment($hook_suffix) {
    if ( !isset($_GET['cancel_appointment']) || $_GET['cancel_appointment'] != '1' || get_post_type() != 'wc_appointment' )
        return;
    drr_cancel_appointment(get_the_ID());
}
add_action('admin_enqueue_scripts', 'drr_admin_cancel_appointment');

/**
 * Cancel appointment from frontend
 */
function drr_frontend_cancel_appointment($hook_suffix) {
    if ( !isset($_GET['cancel_appointment']) || $_GET['cancel_appointment'] != '1' || !is_account_page() || !isset($_GET['appointment_id']) )
        return;
    drr_cancel_appointment($_GET['appointment_id']);
}
add_action('wp_head', 'drr_frontend_cancel_appointment');

/** 
 * Refund an appointment in full
 */
function drr_refund_appointment($appointment, $reason = 'Cancelled by counselor or admin.') {
    $order = $appointment->get_order();
    $refund = wc_create_refund( array(
        'amount'         => $order->get_total(),
        'reason'         => $reason,
        'order_id'       => $order->ID,
        'line_items'     => array(),
        'refund_payment' => false,
        'restock_items'  => true,
    ) );
}

/** 
 * Admin notices for confirmation
 */
function drr_appointment_admin_notices() {
    if (! isset($_GET['wc-appointment-confirmation-status']))
        return;

    $response = $_GET['wc-appointment-confirmation-status'];
    if ($response == 'confirmed') { 
        ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e('Appointment has been confirmed.', 'drr'); ?></p>
        </div>
        <?php
    }
    if ($response == 'cancelled') { 
        ?>
        <div class="notice notice-error is-dismissible">
            <p><?php _e('Appointment has been cancelled.', 'drr'); ?></p>
        </div>
        <?php
    }
}
add_action( 'admin_notices', 'drr_appointment_admin_notices' );

/** 
 * Frontend notices for confirmation
 */
function drr_appointment_frontend_notices() {
    if (! isset($_GET['wc-appointment-confirmation-status']))
        return;

    $response = $_GET['wc-appointment-confirmation-status'];
    if ($response == 'confirmed') { 
        wc_print_notice( __( 'Appointment has been confirmed.', 'woocommerce' ), 'success' );
    }
    if ($response == 'cancelled') { 
        wc_print_notice( __( 'Appointment has been cancelled.', 'woocommerce' ), 'error' );
    }
}
add_action( 'woocommerce_account_content', 'drr_appointment_frontend_notices', 5 );



/**
 * Show appointment accept/cancel links
 */
function drr_appointment_accept_cancel() {
    global $post;
    $appointment_ids = array($post->ID);
    wc_get_template( 'order/admin/appointment-display.php', array( 'appointment_ids' => $appointment_ids ), 'woocommerce-appointments', WC_APPOINTMENTS_TEMPLATE_PATH );
}
function drr_add_appointment_metaboxes() {
	add_meta_box(
		'drr_appointment_accept_cancel',
		'Appointment Confirmation',
		'drr_appointment_accept_cancel',
		'wc_appointment',
		'normal',
		'high'
	);
}
add_action( 'add_meta_boxes', 'drr_add_appointment_metaboxes' );

/**
 * Change date for pending appointments
 */
function drr_change_date_for_pending_appointments($summary) {
    $appointment = $summary['appointment'];
    if ('pending-confirmation' == $appointment->get_status()) {
        $date = get_post_meta( $appointment->get_id(), 'requested_time_slots', true );
        $dates = explode(',', $date);
        foreach ($dates as $key => $date) {
            $dates[$key] = drr_get_user_time(get_current_user_id(), $date);
        }
        $last = array_pop($dates);
        $date = implode(', ', $dates) . ' or ' . $last;
        $summary['date'] = $date;
    }
    return $summary;
}
add_filter('wc_appointments_get_summary_list', 'drr_change_date_for_pending_appointments', 10, 1);

/** 
 * Fix for WC Appointments cart bug
 */
function drr_fix_wc_appointments_cart($labels) {
    return;
}
$cart_manager = $GLOBALS['wc_appointment_cart_manager'];
remove_filter( 'woocommerce_get_item_data', array($cart_manager, 'get_item_data') );

/** 
 * Change request booking text
 */
function drr_change_request_booking_text($text) {
    return 'Request Booking';
}
add_filter('woocommerce_appointment_single_check_availability_text', 'drr_change_request_booking_text', 10, 1);

/**
 * Remove reviews
 */
function drr_remove_reviews_tab($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'drr_remove_reviews_tab', 98 );

/**
 * Add appointment confirmation message to thank you page
 */
function drr_add_appointment_message_to_thank_you($message, $order) {
    $items = $order->get_items(); 
    foreach ( $items as $item ) {
       $product = wc_get_product( $item['product_id'] );
       if ( $product && $product->is_type( 'appointment' ) ) {
           $message = 'Thank you for your booking. You will receive a confirmation email within 48 hours.';
           break;
       }
   }
    return $message;
}
add_filter( 'woocommerce_thankyou_order_received_text', 'drr_add_appointment_message_to_thank_you', 10, 2 );

/**
 * Don't block unconfirmed booking times
 */
function drr_skip_unconfirmed_bookings_availability($ids) {
    if (is_array($ids)) {
        $confirmed_ids = array();
        foreach($ids as $id) {
            $appointment = get_wc_appointment($id);
            if ('pending-confirmation' != $appointment->get_status()) {
                $confirmed_ids[] = $id;
            }
        }
        return $confirmed_ids;
    }
    return $ids;
}
// return add_filter( 'woocommerce_appointments_in_date_range_query', 'drr_skip_unconfirmed_bookings_availability', 10, 1);

/**
 * Prevent pending appointment time from being blocked to other users
 */
function drr_set_booked_appointment_statuses($array) {
    $array = array(
        'unpaid'               => __( 'Unpaid','woocommerce-appointments' ),
        // 'pending-confirmation' => __( 'Pending Confirmation','woocommerce-appointments' ),
        'confirmed'            => __( 'Confirmed','woocommerce-appointments' ),
        'paid'                 => __( 'Paid &amp; Confirmed','woocommerce-appointments' ),
        'complete'             => __( 'Complete','woocommerce-appointments' ),
        'in-cart'              => __( 'In Cart','woocommerce-appointments' ),
    );
    return $array;
}

add_filter( 'woocommerce_appointment_statuses_for_fully_scheduled', 'drr_set_booked_appointment_statuses', 10, 1);

/**
 * Send mentor to reminder email
 */
function drr_send_mentor_to_reminder_email($customer_email, $appointment) {
    $staff_id = $appointment->get_staff_ids()[0];
    $staff_info = get_userdata($staff_id);
    $emails = array($customer_email, $staff_info->user_email);
    $emails = implode($emails, ', ');
    return $emails;
}
add_filter( 'woocommerce_email_reminder_recipients', 'drr_send_mentor_to_reminder_email', 10, 2 );

/**
 * Change time zones to user's local
 */
function drr_change_time_slot_html($slot_html, $slot, $quantity, $time_to_check, $staff_id, $timezone, $appointable_product) {
    // $date = date('c', intval($slot)) . ' ';
    // $time = drr_get_user_time(get_current_user_id(), $date, 'g:ia');
    if ( $quantity['scheduled'] ) {
        $slot_html = "<li class=\"slot$selected\"$slot_locale data-slot=\"" . esc_attr( date( 'Hi', $slot ) ) . "\"><a href=\"#\" data-value=\"" . date_i18n( 'G:i', $slot ) . "\">" . date_i18n( 'g:i a', $slot ) . " <small class=\"spaces-left\">(" . sprintf( _n( '%d left', '%d left', $slot_quantity_available, 'woocommerce-appointments' ), $slot_quantity_available ) . ")</small></a></li>";
    } else {
        $slot_html = "<li class=\"slot$selected\"$slot_locale data-slot=\"" . esc_attr( date( 'Hi', $slot ) ) . "\"><a href=\"#\" data-value=\"" . date_i18n( 'G:i', $slot ) . "\">" . date_i18n( 'g:i a', $slot ) . "</a></li>";
    }
    return $slot_html;
}
add_filter( 'woocommerce_appointments_time_slot_html', 'drr_change_time_slot_html', 10, 7);

/**
 * Show availability in current user timezone
 */
function drr_filter_availability_for_user_timezone($availability_rules, $for_staff, $appointment) {
    $product_rules = $appointment->get_availability();
    $staff_timezone = drr_get_user_timezone($appointment->get_staff_ids()[0]);
    $customer_timezone = drr_get_user_timezone(get_current_user_id());

    $added_days = array();
    foreach($product_rules as $key => $rule) {
        if (substr( $rule['type'], 0, 4 ) === "time" ) {
            $day_numeric = str_replace('time:', '', $rule['type']);
            $dow = date('D', strtotime("Sunday +{$day_numeric} days"));
            $from = new DateTime("{$dow}, {$rule['from']}", new DateTimeZone($staff_timezone) );
            $to = new DateTime("{$dow}, {$rule['to']}", new DateTimeZone($staff_timezone) );
            $from->setTimezone(new DateTimeZone($customer_timezone));
            $to->setTimezone(new DateTimeZone($customer_timezone));
            $product_rules[$key]['type'] = 'time:' . $from->format('N');
            $product_rules[$key]['from'] = $from->format('H:i');
            $product_rules[$key]['to'] = $to->format('H:i');
            // Next day
            if ($to->format('N') != $from->format('N')) {
                $product_rules[$key]['to'] = '0:00';                
                $next_day = array();
                $next_day['appointable'] = 'yes';
                $next_day['type'] = 'time:' . $to->format('N');
                $next_day['qty'] = $product_rules[$key]['qty'];   
                $next_day['from'] = '0:00';    
                $next_day['to'] = $to->format('H:i');
                $added_days[] = $next_day;           
            }
        }
    }
    $product_rules = array_merge(
        $product_rules,
        $added_days
    );

    $availability_rules = array_filter(
        array_merge(
            // WC_Product_Appointment_Rule_Manager::process_availability_rules( array_reverse( $global_rules ), 'global' ),
            WC_Product_Appointment_Rule_Manager::process_availability_rules( array_reverse( $product_rules ), 'product' )
            // WC_Product_Appointment_Rule_Manager::process_availability_rules( array_reverse( $staff_rules ), 'staff' )
        )
    );

    usort( $availability_rules, array( $appointment, 'rule_override_power_sort' ) );
    return $availability_rules;

}
add_filter('woocommerce_appointment_get_availability_rules', 'drr_filter_availability_for_user_timezone', 10, 3);

/**
 * Disable ICS card attachment
 */
add_action( 'init', 'drr_remove_ics_attachment' );
function drr_remove_ics_attachment() {
    remove_filter( 'woocommerce_email_attachments', array( 'WC_Appointments_Email_Manager', 'attach_ics_file' ), 20 );
}

/**
 * Filter addon option text
 */
function drr_addon_option_text( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
		case 'Select an option...' :
			$translated_text = __( 'Select length of session', 'drr' );
			break;
	}
	return $translated_text;
}
add_filter( 'gettext', 'drr_addon_option_text', 20, 3 );