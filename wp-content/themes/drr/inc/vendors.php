<?php

/**
 * Get total of order for vendor commission (including addons)
 */
function drr_calculate_vendor_commission_total($commission, $product_id, $term_id, $variation_id, $item_id, $order) {
    return $commission;
}
add_filter('wcmp_get_commission_amount', 'drr_calculate_vendor_commission_total', 10, 6);

/**
 * Remove vendor weekly emails
 */
remove_action('vendor_weekly_order_stats', array('WCMp_Cron_Job', 'vendor_weekly_order_stats_report'), 20);

/**
 * Change vendor commission after first order
 */
function drr_change_vendor_commission( $order_id ){
    $order = wc_get_order( $order_id );
    $items = $order->get_items();
    foreach( $items as $item ) {
        $product_id = $item->get_product_id();
    }
    $units_sold = get_post_meta( $product_id, 'total_sales', true );
    if( $units_sold == 1 ) {
        update_post_meta( $product_id, '_commission_percentage_per_product', '0.0' );
        update_post_meta( $product_id, '_commission_fixed_with_percentage', '12' );
    }
}
add_action( 'woocommerce_order_status_completed', 'drr_change_vendor_commission' );
