<?php

/**
 * Register new endpoint to use inside My Account page.
 *
 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
 */
function drr_custom_endpoints() {
	add_rewrite_endpoint( 'emergency-session', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'upcoming-sessions', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'previous-sessions', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'available-credits', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'stats', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'payout-settings', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'edit-profile', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'drr_custom_endpoints' );

/**
 * Add new query var.
 *
 * @param array $vars
 * @return array
 */
function drr_custom_query_vars( $vars ) {
	$vars[] = 'emergency-session';
	$vars[] = 'upcoming-sessions';
	$vars[] = 'previous-sessions';
	$vars[] = 'available-credits';
	$vars[] = 'stats';
	$vars[] = 'payout-settings';
	$vars[] = 'edit-profile';

	return $vars;
}

add_filter( 'query_vars', 'drr_custom_query_vars', 0 );

/**
 * Change my account tab
 */
function drr_my_account_tabs( $items ) {
	$user = wp_get_current_user();
 	$newItems = $items;
    // Change for all
    unset($newItems['appointments']);     
    unset($newItems['payment-methods']);
    unset($newItems['customer-logout']);
    unset($newItems['dashboard']);
    unset($newItems['edit-address']);
    unset($newItems['downloads']);
    unset($newItems['edit-account']);
    // $newItems['upcoming-sessions'] = 'Upcoming Sessions';  
    $newItems = array('upcoming-sessions' => 'Upcoming Sessions') + $newItems;
    $newItems['previous-sessions'] = 'History';  
	if (in_array('dc_vendor', $user->roles, true)) {
        unset($newItems['customer-logout']);    
        unset($newItems['orders']);    
        unset($newItems['edit-account']);    
        $newItems['stats'] = 'Stats';  
        $newItems['payout-settings'] = 'Payout Settings';
        $newItems['emergency-session'] = 'Emergency Session';
        // $newItems['edit-profile'] = 'Edit Profile';
    }
    if (!in_array('dc_vendor', $user->roles, true)) {
        $newItems['available-credits'] = 'Available Credits';
    }
    $newItems['edit-account'] = 'Account Details';
    return $newItems;
}
add_filter( 'woocommerce_account_menu_items', 'drr_my_account_tabs' );

/**
 * 
 */
function drr_add_avatar() {
    echo '<div class="avatar-wrapper">';
    echo um_user('profile_photo', 200);
    echo '</div>';
}
add_action( 'woocommerce_account_navigation', 'drr_add_avatar', 1 );

/** 
 * Previous staff appointments content
 */
function drr_previous_appointments_content() {

    $user = wp_get_current_user();
    
    if ( version_compare( WC()->version, '2.6.0', '>=' ) ) {

        if (in_array('dc_vendor', $user->roles, true)) {
            $all_appointments = WC_Appointments_Controller::get_appointments_for_staff( $user->ID, array('confirmed', 'paid') );
        } else {
			$all_appointments = WC_Appointments_Controller::get_appointments_for_user( $user->ID, array(
				'order_by'     => apply_filters( 'woocommerce_appointments_my_appointments_past_order_by', 'start_date' ),
				'order'       => 'ASC',
			) );
        }

        $past_appointments = array_filter($all_appointments, function($appointment){
            $now = new DateTime();
            return $appointment->end < $now->getTimestamp();
        });
        
        $tables = array();
        if ( ! empty( $past_appointments ) ) {
            $tables['past'] = array(
                'header'   => __( 'Past Sessions', 'woocommerce-appointments' ),
                'appointments' => $past_appointments,
            );
        }
    }
    include( locate_template( 'templates/my-account/sessions.php' ) );
}
add_action( 'woocommerce_account_previous-sessions_endpoint', 'drr_previous_appointments_content' );

/** 
 * Upcoming staff appointments content
 */
function drr_upcoming_appointments_content() {

    $user = wp_get_current_user();
    
    if ( version_compare( WC()->version, '2.6.0', '>=' ) ) {

        if (in_array('dc_vendor', $user->roles, true)) {
            $all_appointments = WC_Appointments_Controller::get_appointments_for_staff( $user->ID, array('confirmed', 'paid') );
        } else {
            $all_appointments = WC_Appointments_Controller::get_appointments_for_user( $user->ID, array(
                'order_by'    => apply_filters( 'woocommerce_appointments_my_appointments_upcoming_order_by', 'start_date' ),
                'order'      => 'ASC',
                // 'date_after' => current_time( 'timestamp' ),
            ) );
        }

        $upcoming_appointments = array_filter($all_appointments, function($appointment){
            $now = new DateTime();
            return $appointment->end > $now->getTimestamp();
        });

        $tables = array();
        if ( ! empty( $upcoming_appointments ) ) {
            $tables['upcoming'] = array(
                'header'   => __( 'Upcoming Sessions', 'woocommerce-appointments' ),
                'appointments' => $upcoming_appointments,
            );
        }
    }
    include( locate_template( 'templates/my-account/sessions.php' ) );
}
add_action( 'woocommerce_account_upcoming-sessions_endpoint', 'drr_upcoming_appointments_content' );

/** 
 * Emergency session tab content
 */
function drr_emergency_session_content() {
    $args = array(
        'name'        => 'emergency-session',
        'post_type'   => 'page',
        'numberposts' => 1
    );
    $posts = get_posts($args);
    if ($posts) {
        echo $posts[0]->post_content;
    }
}
add_action( 'woocommerce_account_emergency-session_endpoint', 'drr_emergency_session_content' );

/**
 * Add current session notice for users
 */
function drr_current_session_notice($content) {
    if ($user_id = get_current_user_id()) {
        $current_appointments = WC_Appointments_Controller::get_appointments_for_user( $user_id, array(
            'order_by'    => apply_filters( 'woocommerce_appointments_my_appointments_upcoming_order_by', 'start_date' ),
            'order'      => 'ASC',
            'date_between' => array(
                'start' => time() - (60 * 60 * 3),
                'end'   => time() + (60 * 60 * 3),
            ),
        ) );
    
        if ($current_appointments) {
            foreach ($current_appointments as $appointment) {
                if (time() >= $appointment->get_start() - (60 * 10) && time() <= $appointment->get_end() + (60 * 60)) {
                    $content = '<div class="alert alert-info session-notice">Your virtual room is ready. <a href="'. home_url( '/session?session_id=' . $appointment->ID ) . '" style="text-decoration: underline;">Enter Session</a></div>' . $content;                
                }
            }
        }
    }
    return $content;
}
add_action( 'the_content', 'drr_current_session_notice', 10, 1 );


/** 
 * Previous staff appointments content
 */
function drr_available_credits_content() {
    echo '<h3>Current Balance</h3>';
    echo do_shortcode('[uw_balance display_username="false" separator=":" username_type="display_name"]');
    echo '<h3 class="mt-5">Add Credits</h3>';
    echo do_shortcode('[uwcs_dynamic_deposit_template button_text="Confirm Amount" description="Or pick your own deposit amount" default_amount="60"]');
}
add_action( 'woocommerce_account_available-credits_endpoint', 'drr_available_credits_content' );

/*
*	Add a hidden field to our WooCommerce login form - passing in the refering page URL
*	Note: the input (hidden) field doesn't actually get created unless the user was directed
*	to this page from a single product page
*/
function drr_add_referer_field() {

    $referer = wp_get_referer();
    if (isset($_GET['redirect_to'])) {
        $referer = $_GET['redirect_to'];
    }
  
	if( $referer ) {
        ?>
            <input type="hidden" name="redirect-user" value="<?php echo $referer; ?>">
        <?php
	}
}
add_action( 'woocommerce_login_form', 'drr_add_referer_field' );
add_action( 'woocommerce_register_form_start', 'drr_add_referer_field' );

/*
*	Redirect the user back to the passed in referer page
*	- Which should be the URL to the last viewed product before logging
*/
function drr_redirect_to_referer( $redirect, $user ) {
    if( isset( $_POST['redirect-user'] ) ) {
		$redirect = urldecode($_POST['redirect-user']);
        return $redirect;
	}
}
add_filter( 'woocommerce_login_redirect', 'drr_redirect_to_referer', 15, 2 );

/** 
 * Vendor order stats
 */
function drr_vendor_order_stats_content() {
    global $WCMp;
    if (isset($_POST['wcmp_stat_start_dt'])) {
        $start_date = $_POST['wcmp_stat_start_dt'];
    } else {
        // hard-coded '01' for first day     
        $start_date = date('01-m-Y');
    }

    if (isset($_POST['wcmp_stat_end_dt'])) {
        $end_date = $_POST['wcmp_stat_end_dt'];
    } else {
        // hard-coded '01' for first day
        $end_date = date('t-m-Y');
    }
    $vendor = get_wcmp_vendor(get_current_vendor_id());
    $WCMp_Plugin_Post_Reports = new WCMp_Report();
    $array_report = $WCMp_Plugin_Post_Reports->vendor_sales_stat_overview($vendor, $start_date, $end_date);
    $WCMp->template->get_template('vendor-dashboard/vendor-report.php', $array_report);
}
add_action( 'woocommerce_account_stats_endpoint', 'drr_vendor_order_stats_content' );

/** 
 * Vendor billing / payout content
 */
function drr_vendor_billing_content() {
    global $WCMp;
    $vendor = get_wcmp_vendor(get_current_vendor_id());
    $user_array = $WCMp->user->get_vendor_fields($vendor->id);
    $WCMp->template->get_template('vendor-dashboard/vendor-billing.php', $user_array);
    ?>
    <script>
    jQuery('input, select').each(function(){
			jQuery(this).prop('disabled', false);
        });
    </script>
    <?php
}
add_action( 'woocommerce_account_payout-settings_endpoint', 'drr_vendor_billing_content' );

/**
 * Save vendor billing info
 */
function drr_save_vendor_billing() {
    if(isset($_POST['vendor_payment_mode'])) {
        update_user_meta(get_current_vendor_id(), '_vendor_payment_mode', sanitize_text_field($_POST['vendor_payment_mode']));
    }
}
add_action( 'init', 'drr_save_vendor_billing', 1 );

/**
 * Add account styles and scripts
 */
function drr_account_styles_and_scripts() {
    if (is_account_page()) {
        global $WCMp;
        $frontend_style_path = $WCMp->plugin_url . 'assets/frontend/css/';
        wp_enqueue_style('dashicons');
        wp_enqueue_style('jquery-ui-style');
        wp_enqueue_style('wcmp_new_vandor_dashboard_css', $frontend_style_path . 'vendor_dashboard.min.css', array(), $WCMp->version);
        wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css', array(), $WCMp->version);
        $frontend_script_path = $WCMp->plugin_url . 'assets/frontend/js/';        
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('wcmp_new_vandor_dashboard_js', $frontend_script_path . '/vendor_dashboard.min.js', array('jquery'), $WCMp->version, true);
        wp_enqueue_script('wcmp_profile_edit_js', $frontend_script_path . '/profile_edit' . $suffix . '.js', array('jquery'), $WCMp->version, true);    
    }
}
add_action( 'wp_enqueue_scripts', 'drr_account_styles_and_scripts' );

/**
 * Redirect edit profile to UM profile
 */
function drr_redirects() {
    global $wp_query;
    if ( isset( $wp_query->query_vars['edit-profile'] ) ) {
        wp_redirect( um_edit_profile_url() );
        die;
    }
}
add_action('template_redirect', 'drr_redirects');

/**
 * Redirect UM account to WC my account
 */
function drr_redirect_um_account_to_wc($url, $slug, $updated) {
    if ($slug == 'account') {
        $url = get_permalink( get_option('woocommerce_myaccount_page_id') );
    }
    return $url;
}
add_filter('um_get_core_page_filter', 'drr_redirect_um_account_to_wc', 10, 3);