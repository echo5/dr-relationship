<?php

/**
 * Auto set new booking products to no referral commission
 */
function drr_set_bookings_default_no_commission() {
    if ('product' != get_post_type()) :
        return;
    endif;
    ?>
    <script type='text/javascript'>
        $(document).ready(function () {
            $('#product-type').change(function(){
                if ($(this).val() == 'appointment') {
                    $('#_affwp_woocommerce_referrals_disabled').prop('checked', true);
                }
            });
        });
    </script>
    <?php
}
add_action('admin_footer', 'drr_set_bookings_default_no_commission');


/**
 * Pay referrals by site credits
 */
function drr_pay_referral_by_credits($referral_id, $referral, $reference) {

    $user_id = affwp_get_affiliate_user_id( $referral->affiliate_id );
    drr_add_user_credits($user_id, $referral->amount);

    $args = array(
        'affiliate_id'  => $referral->affiliate_id,
        'referrals'     => array($referral_id),
        'amount'        => $referral->amount,
        'payout_method' => 'credits',
        'owner'         => get_current_user_id(),
        'status'        => 'paid',
    );
    $payouts = new Affiliate_WP_Payouts_DB;
    $payouts->add($args);

}
add_action( 'affwp_complete_referral', 'drr_pay_referral_by_credits', 10, 3 );