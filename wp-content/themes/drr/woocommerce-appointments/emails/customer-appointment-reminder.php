<?php
/**
 * Customer appointment reminder email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-appointment-reminder.php.
 *
 * HOWEVER, on occasion we will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @version     3.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<?php if ( $appointment->get_start_date( wc_date_format(), '' ) == date( wc_date_format() ) ) : ?>
	<p><?php _e( 'This is a reminder that your appointment will take place today.', 'woocommerce-appointments' ); ?></p>
<?php else : ?>
	<p><?php _e( 'This is a reminder that your appointment will take place tomorrow.', 'woocommerce-appointments' ); ?></p>
<?php endif; ?>

<p><?php _e( 'Please use the following link to enter the video session.  This link will also be shown as ENTER SESSION under My Account->Upcoming Sessions 5 minutes before the appointment start time.', 'woocommerce-appointments' ); ?></p>
<p>
	<a href="<?php echo home_url( '/session?session_id=' . absint( $appointment->ID ) ); ?>" class="button button-success"><?php _e('Enter Session', 'drr'); ?></a><br/>
	<?php echo home_url( '/session?session_id=' . absint( $appointment->ID ) ); ?>
</p>


<table style="border:1px solid #eee; width:100%;" cellspacing="0" cellpadding="6" border="1" bordercolor="#eee">
	<tbody>
		<tr>
			<th style="text-align:left; border:1px solid #eee;" scope="row"><?php _e( 'Appointment ID', 'woocommerce-appointments' ); ?></th>
			<td style="text-align:left; border:1px solid #eee;"><?php echo $appointment->get_id(); ?></td>
		</tr>
		<tr>
			<th style="text-align:left; border:1px solid #eee;" scope="row"><?php _e( 'Appointment Date', 'woocommerce-appointments' ); ?></th>
			<td style="text-align:left; border:1px solid #eee;">
				<?php $customer_date = drr_get_user_time($appointment->get_customer_id(), $appointment->get_start_date()); ?>
				<?php $mentor_date = drr_get_user_time($appointment->get_staff_ids()[0], $appointment->get_start_date()); ?>
				<?php echo $customer_date; ?>
				<?php echo $customer_date == $mentor_date ? '' : '<br>' . $mentor_date; ?>
			</td>
		</tr>
		<tr>
			<th style="text-align:left; border:1px solid #eee;" scope="row"><?php _e( 'Appointment Duration', 'woocommerce-appointments' ); ?></th>
			<td style="text-align:left; border:1px solid #eee;"><?php echo $appointment->get_duration(); ?></td>
		</tr>
		<?php if ( $appointment->has_staff() && ( $staff = $appointment->get_staff_members( $names = true ) ) ) : ?>
			<tr>
				<th style="text-align:left; border:1px solid #eee;" scope="row"><?php _e( 'Appointment Providers', 'woocommerce-appointments' ); ?></th>
				<td style="text-align:left; border:1px solid #eee;"><?php echo $staff; ?></td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>

<?php do_action( 'woocommerce_email_footer' ); ?>
