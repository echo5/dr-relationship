<?php
/**
 * Admin new appointment email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/admin-new-appointment.php.
 *
 * HOWEVER, on occasion we will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @version     3.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<?php do_action( 'woocommerce_email_header', $email_heading );

$order = $appointment->get_order();

if ( $order ) {
	if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
		$first_name = $order->billing_first_name;
		$last_name = $order->billing_last_name;
	} else {
		$first_name = $order->get_billing_first_name();
		$last_name = $order->get_billing_last_name();
	}
}
?>

<?php
if ( wc_appointment_order_requires_confirmation( $appointment->get_order() ) && $appointment->get_status() == 'pending-confirmation' ) {
	$opening_paragraph = __( 'An appointment has been made by %s and is awaiting your approval. The details of this appointment are as follows:', 'woocommerce-appointments' );
} else {
	$opening_paragraph = __( 'An new appointment has been made by %s. The details of this appointment are as follows:', 'woocommerce-appointments' );
}
?>

<?php if ( $appointment->get_order() && ! empty( $first_name ) && ! empty( $last_name ) ) : ?>
	<p><?php printf( $opening_paragraph, $first_name . ' ' . $last_name ); ?></p>
<?php endif; ?>

<table style="border:1px solid #eee; margin:0 0 16px; width:100%;" cellspacing="0" cellpadding="6" border="1" bordercolor="#eee">
	<tbody>
		<tr>
			<th style="text-align:left; border:1px solid #eee;" scope="row" ><?php _e( 'Scheduled Mentor', 'woocommerce-appointments' ); ?></th>
			<td style="text-align:left; border:1px solid #eee;"><?php echo $appointment->get_product()->get_title(); ?></td>
		</tr>
		<tr>
			<th style="text-align:left; border:1px solid #eee;" scope="row"><?php _e( 'Appointment ID', 'woocommerce-appointments' ); ?></th>
			<td style="text-align:left; border:1px solid #eee;"><?php echo $appointment->get_id(); ?></td>
		</tr>
		<tr>
			<th style="text-align:left; border:1px solid #eee;" scope="row"><?php _e( 'Appointment Duration', 'woocommerce-appointments' ); ?></th>
			<td style="text-align:left; border:1px solid #eee;"><?php echo $appointment->get_duration(); ?></td>
		</tr>
		<?php if ( $appointment->has_staff() && ( $staff = $appointment->get_staff_members( $names = true ) ) ) : ?>
			<tr>
				<th style="text-align:left; border:1px solid #eee;" scope="row"><?php _e( 'Appointment Providers', 'woocommerce-appointments' ); ?></th>
				<td style="text-align:left; border:1px solid #eee;"><?php echo $staff; ?></td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>

<?php if ( wc_appointment_order_requires_confirmation( $appointment->get_order() ) && $appointment->get_status() == 'pending-confirmation' ) : ?>
<p><?php _e( 'This appointment is awaiting your approval. Please click on any of the requested time slots to confirm the appointment or click "Cancel Appointment" if you\'re unavailable.', 'woocommerce-appointments' ); ?></p>
<?php endif; ?>

<?php
$time_slots = drr_get_appointment_time_slots($appointment->get_id());
foreach ($time_slots as $key => $time_slot):
	?>
	<p style="float:left;text-align:center;width:33.33%;margin-bottom:10px;">	
		<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ) . '/upcoming-sessions?appointment_id=' . absint( $appointment->get_id() ) . '&appointment_slot=' . absint( $key ); ?>" style="background: #f7f7f7;border: 1px solid #cccccc;padding: 10px;text-decoration: none;border-radius: 5px;display: inline-block;"><?php echo $time_slot; ?></a>
	</p>
	<?php
endforeach;
?>
<p>
	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ) . '/upcoming-sessions?appointment_id=' . absint( $appointment->get_id() ) . '&cancel_appointment=1'; ?>" class="button button-warning button-danger button-red"><?php _e('Cancel Appointment', 'drr'); ?></a>
</p>

<?php do_action( 'woocommerce_email_footer' ); ?>
